#!/bin/bash

#FAIL2BAN SORTING ROUTINES

#AUTHOR: Ben Burk
#DATE: 2019.08

#This really isn't necessary. One could just use the .sqlite database created by fail2ban.
#However, I have modified my fail2ban setup, so this is necessary

if ! [[ $(id -u) = 0 ]]; then
	echo -e "\033[1;31mERROR! Fail2ban resorter script must be called as root\033[0m"
	exit
fi

email_report() {

	if [[ "$(systemctl is-active fail2ban)" == "inactive" ]]; then
		echo -e "$(date +%c)\n\nSTATUS: OFFLINE (re-sorted)...\nCould not be restarted after blacklist sorting!\n\n $(journalctl --unit=fail2ban.service --since=$(date --date='5 minutes ago' '+%H:%M:%S') --no-pager)" | mail -r FROM_ADDRESS -s "CRON: FAIL2BAN REPORT - $(date +%F) : OFFLINE" TO_ADDRESS
		exit 1
	else
		echo -e "$(date +%c)\n\nSTATUS : RESTARTING (re-sorted)...\nPERSISTENCE : $(cat /etc/fail2ban/persistent.bans | wc -l) BLOCKED IPS.\n" | mail -r FROM_ADDRESS -s "CRON: FAIL2BAN REPORT - $(date +%F) : ONLINE" TO_ADDRESS
	fi

}

echo "$(date)=====================================================================" >>/etc/fail2ban/persistent.history
if [[ "$(systemctl is-active fail2ban)" == "active" ]]; then
	systemctl stop fail2ban
else
	echo -e "Fail2ban is not running! exiting...\n" >>/etc/fail2ban/persistent.history ; email_report ; exit 1
fi

change=$(diff /etc/fail2ban/persistent.sorted /etc/fail2ban/persistent.bans)
if [[ -z $change ]]; then
	echo -e "No difference(s) found\n" >>/etc/fail2ban/persistent.history
else
	echo -e "$change\n" >>/etc/fail2ban/persistent.history
fi

cat /etc/fail2ban/persistent.bans | sort | uniq -u >/etc/fail2ban/persistent.sorted
cat /etc/fail2ban/persistent.sorted >/etc/fail2ban/persistent.bans
systemctl start fail2ban && sleep 3
email_report
