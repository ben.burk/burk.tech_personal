#!/bin/bash

#BATCH EMAILER

#AUTHOR: Ben Burk
#DATE: 2020.05

#REQUIREMENTS
#s-nail

from=""
to=""
smtpserver=""
smtpport=
user=""
password=""

SUBJECT_PREFIX=""
BODY_PREFIX=""
ATTACHMENT_LIMIT=

declare -a attachments
attachments=( $( ls *.png ) )
alen=${#attachments[@]}

[[ $((alen % ATTACHMENT_LIMIT)) -ne 0 ]] && ((g=alen/ATTACHMENT_LIMIT,h=g+1)) || h=$((alen/ATTACHMENT_LIMIT))
for ((i=0;i<$h;i++)); do
	declare -a attargs
	j=$((i*ATTACHMENT_LIMIT))
	[[ $((j+ATTACHMENT_LIMIT)) -gt ${#attachments[@]} ]] && k=${#attachments[@]} || k=$((j+ATTACHMENT_LIMIT))

	for ((l=$j;l<$k;l++)); do
		attargs+=( "-a" "${attachments[${l}]}" )
	done

	subject="${SUBJECT_PREFIX} $((i+1))"
	body="${BODY_PREFIX} $((i+1))"
	
	s-nail -s "${subject}" -r "${from}" -S smtp="smtp://${smtpserver}:${smtpport}" \
                              -S smtp-auth=plain \
			      -S smtp-use-starttls \
                              -S smtp-auth-user="${user}" \
                              -S smtp-auth-password="${password}" \
                              -S sendwait \
                              "${attargs[@]}" "${to}" <<< "${body}"
	
	unset attargs
	sleep 30
done
