#!/bin/bash

#DYNAMIC CLOUDFLARE DKIM UPDATER
#Written against API v4.
#Borrowed from 'DYNAMIC CLOUDFLARE DNS UPDATER'

#AUTHOR: Ben Burk
#DATE: 2019.09

#REQUIREMENTS
#grep
#sed
#cut
#opendkim
#tail
#head
#at
#curl
#tr
#jq
#awk

#ensure script is called as root
if ! [ $(id -u) = 0 ]; then
	echo -e "\033[31;1mERROR! Cloudflare Dynamic DKIM script started without root permissions!\033[0m"
	exit
fi 

#INCLUSION 2019.09: getparams. Addition to allow fields from API file as strings surrounded by single quotes. Legacy support as well for strings without.
#INCLUSION 2019.09: Scheduled DNS record revocation using atd. Records are removed 24 hours after creation/implementation of new key
#INCLUSION 2019.10: getparams. Addition to allow fields from API file as strings surrounded by double quotes. Legacy support as well for strings without.

#User-definable variables:	THESE MUST BE DEFINED BEFORE EXECUTION!
#CF_FILE: Full path to file containing cloudflare API data
#	FORMAT:
#
#	CF_Key="API_KEY", 'API_KEY', or API_KEY		<--- YOUR CF API KEY goes here
#	CF_Email="API_KEY", 'API_EMAIL', or API_EMAIL	<--- YOUR CF API EMAIL goes here
#
#CF_DOMAIN_NAME : Your domain
#CF_RECORD_NAME : DNS record key, i.e. name of record. For DKIM, the name should be something like 'selector._domainkey.your.domain'
#CF_RECORD_TYPE : DNS record type, {A, CNAME, etc}. For DKIM, the type should be 'TXT'
#DKIM_PD : Full path to OpenDKIM parent directory
#DKIM_ST : Full path to OpenDKIM signing table
#DKIM_KT : Full path to OpenDKIM key table
#DKIM_KSZ : Key size for OpenDKIM key
#CFDYN_LOGFILE : Full path to logfile

#WARNING : ENSURE TO CLEANUP ANY LEFTOVER ATD JOBS THAT MAY EXIST IF THIS SCRIPT IS MOVED BETWEEN CALLS!

CF_API_KEY=null
CF_API_EMAIL=null
CF_FILE=null
CF_ID=null
CF_DOMAIN_NAME=null
CF_RECORD_ID=null
CF_RESPONSE=null
CF_RECORD_NAME=null
CF_RECORD_TYPE=null
DKIM_PD=null
DKIM_ST=null
DKIM_KT=null
DKIM_KSZ=null
CFDYN_LOGFILE=/dev/null		#set this to /dev/null for no output, or /dev/stdout for STDOUT

echo -e "$(date +%c) - START =====================================================================" >>"$CFDYN_LOGFILE"

exit_script() {
	
	echo -e "$(date +%c) - STOP ======================================================================" >>"$CFDYN_LOGFILE"
	exit
}

get_params() {

	if [ ! -f "$CF_FILE" ]; then
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE does not exist!" >>"$CFDYN_LOGFILE"
		exit 1
	fi

	grep -q "^CF_Key=" "$CF_FILE" &>/dev/null
        cf_key_chk=$?
	if [ $cf_key_chk -eq 1 ]; then	
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE missing CF_Key!" >>"$CFDYN_LOGFILE"
		exit 1
	else
		CF_API_KEY="$(grep "CF_Key" "$CF_FILE" | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" #permit field in file be surrounded by single, double quotes
	fi
	grep -q "^CF_Email=" "$CF_FILE" &>/dev/null
	cf_email_chk=$?
	if [ $cf_email_chk -eq 1 ]; then
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE missing CF_Email!" >>"$CFDYN_LOGFILE"
		exit 1
	else
		CF_API_EMAIL="$(grep "CF_Email" "$CF_FILE" | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" #permit field in file be surrounded by single, double quotes
	fi

}

create_dkim_key() {

	opendkim-genkey -r -s "$(echo $CF_RECORD_NAME | sed 's/\..*//')" -b $DKIM_KSZ -d "$CF_DOMAIN_NAME" -D "$DKIM_PD" >>"$CFDYN_LOGFILE"
	chown opendkim:opendkim "$DKIM_PD/$(echo $CF_RECORD_NAME | sed 's/\..*//')".*
	grep -q "$CF_RECORD_NAME\t$CF_DOMAIN_NAME:$(echo $CF_RECORD_NAME | sed 's/\..*//'):$DKIM_PD/$(echo $CF_RECORD_NAME | sed 's/\..*//').private" "$DKIM_KT"
	dkim_kt_chk=$?
	if [ $dkim_kt_chk -eq 1 ]; then
		echo -e "$CF_RECORD_NAME\t$CF_DOMAIN_NAME:$(echo $CF_RECORD_NAME | sed 's/\..*//'):$DKIM_PD/$(echo $CF_RECORD_NAME | sed 's/\..*//').private" >>"$DKIM_KT"
	else
		echo -e "$(date +%c) - INFO! OpenDKIM KeyTable is already configured to use $CF_RECORD_NAME..." >>"$CFDYN_LOGFILE" 
	fi
	grep -q '*@'"$CF_DOMAIN_NAME\t$CF_RECORD_NAME" "$DKIM_ST"
	dkim_st_chk=$?
	if [ $dkim_st_chk -eq 1 ]; then
		echo -e '*@'"$CF_DOMAIN_NAME\t$CF_RECORD_NAME" >>"$DKIM_ST"
	else
		echo -e "$(date +%c) - INFO! OpenDKIM SigningTable is already configured to use $CF_RECORD_NAME..." >>"$CFDYN_LOGFILE"
	fi

}

#ARGUMENTS : Key name to purge
#RETURNS : None
purge_dkim_key() {

	sed -i "/$1/d" "$DKIM_KT"
	sed -i "/$1/d" "$DKIM_ST"
	rm -f "$DKIM_PD/$(echo $1 | sed 's/\..*//')".* >/dev/null 2>&1	#soft fail. Don't print.

}

#ARGUMENTS : Newline-separated list of current ATD job numbers.
#RETURNS : None
purge_atd_jobs() {

	IFSBAK=$IFS		#backup and set IFS to search for newlines. Restore after the array is created.
	IFS=$'\n'
	atd_jobnum_list=($1)
	IFS=$IFSBAK
				#print operation and delete all atd jobs that were created by this script
	for (( i=0;i<${#atd_jobnum_list[@]};i++ )); do
		if [[ $(at -c ${atd_jobnum_list[$i]} | grep -q "$0") -eq 0 ]]; then
			echo -e "$(date +%c) - Purging ATD job #${atd_jobnum_list[${i}]} : $(at -c ${atd_jobnum_list[${i}]} | tail -2 | head -1)" >>"$CFDYN_LOGFILE"
			atrm ${atd_jobnum_list[$i]}
		fi
	done
}

reload_mail_services() {

	systemctl reload opendkim

}

get_domain_id() {

	CF_ID="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones" \
		     -H "Content-Type:application/json" \
		     -H "X-Auth-Key:$CF_API_KEY" \
		     -H "X-Auth-Email:$CF_API_EMAIL" | jq -r '.result[].id')"

}

find_existing_records() {

	CF_RESPONSE="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$CF_ID/dns_records?type=$CF_RECORD_TYPE&page=1&per_page=20&order=type&direction=desc&match=all" \
		     -H "Content-Type:application/json" \
	             -H "X-Auth-Key:$CF_API_KEY" \
		     -H "X-Auth-Email:$CF_API_EMAIL" | jq -r '.result[].name // empty' | grep "._domainkey." - | tr " " "\n")"

}

#ARGUMENTS : DNS record name to remove
#RETURNS : None
remove_record() {

	#this method to be called only from queued deletion (atd) or emergency purge
	#grab cloudflare record id
	CF_RECORD_ID="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/$CF_ID/dns_records?type=$CF_RECORD_TYPE&name=$1&page=1&per_page=20&order=type&direction=desc&match=all" \
		     -H "X-Auth-Email: $CF_API_EMAIL" \
		     -H "X-Auth-Key: $CF_API_KEY" \
		     -H "Content-Type: application/json" | jq -r '.result[].id')"

	#remove existing record using obtained id
	echo -e "$(date +%c) - Removing record $1 ..." >>"$CFDYN_LOGFILE"
	curl -s -X DELETE "https://api.cloudflare.com/client/v4/zones/$CF_ID/dns_records/$CF_RECORD_ID" \
		     -H "X-Auth-Email: $CF_API_EMAIL" \
		     -H "X-Auth-Key: $CF_API_KEY" \
		     -H "Content-Type: application/json" \
	      	     | jq '.' >>"$CFDYN_LOGFILE"

}

add_record() {

	#add new record and print
	create_dkim_key

	CF_DKIM_RECORD="$(cat "$DKIM_PD/$(echo $CF_RECORD_NAME | sed 's/\..*//').txt" | cut -d"(" -f2 | cut -d")" -f1 | awk '{$1=$1};1' | tr -d '\t\"\n')"
	if [[ "$CF_DKIM_RECORD" == "" ]]; then echo "Could not find DNS record created by opendkim! Exiting..." >>"$CFDYN_LOGFILE" ; exit_script
	else
		echo -e "$(date +%c) - Adding new DKIM record $CF_RECORD_NAME ..." >>"$CFDYN_LOGFILE"
		curl -s -X POST "https://api.cloudflare.com/client/v4/zones/$CF_ID/dns_records" \
	   	     	-H "X-Auth-Email: $CF_API_EMAIL" \
	   	     	-H "X-Auth-Key: $CF_API_KEY" \
	   	     	-H "Content-Type: application/json" \
			--data "{\"type\":\"$CF_RECORD_TYPE\",\"name\":\"$CF_RECORD_NAME\",\"content\":\"$CF_DKIM_RECORD\",\"ttl\":1,\"priority\":10,\"proxied\":false}" \
	   	     	| jq '.' >>"$CFDYN_LOGFILE"
	fi

}


#getopt parsing. processing possible dns record removal
while getopts ":r:" opt; do
	case ${opt} in
		r )
			echo -e "$(date +%c) - Starting queued DKIM record removal..." >>"$CFDYN_LOGFILE"
			get_params ; get_domain_id ; remove_record "$OPTARG"
			exit_script
			;;
		: )
			echo -e "$(date +%c) - ERROR! Queued record removal requires an argument!" >>"$CFDYN_LOGFILE"
			exit_script
			;;
	esac
done
shift $((OPTIND -1))
			

#grab api key and email from file
get_params

#grab domain id
get_domain_id

#query cloudflare for existing records
find_existing_records

record_num=$(echo "$CF_RESPONSE" | wc -l)

if [[ -z $CF_RESPONSE ]]; then							#empty response detected. no DKIM records, so add one
	echo -e "$(date +%c) - No $CF_RECORD_TYPE record found for *._domainkey.*  Adding..." >>"$CFDYN_LOGFILE"
	add_record ; reload_mail_services
elif [ $record_num -lt 2 ] && [[ ! -z $CF_RESPONSE ]]; then			#previous key found. start rotation process
	if [[ $CF_RESPONSE == $CF_RECORD_NAME ]]; then echo -e "$(date +%c) - Will not add new DKIM record with existing selector! Exiting..." >>"$CFDYN_LOGFILE"; exit_script ; fi
	echo -e "$(date +%c) - Starting DKIM rotation..." >>"$CFDYN_LOGFILE"
	add_record ; purge_dkim_key "$CF_RESPONSE" ; reload_mail_services	#postfix begins using new key here
	echo -e "$(date +%c) - Scheduling ATD job for removal of previous record... $(date --date="24hour") ::  $0 -r $CF_RESPONSE..." >>"$CFDYN_LOGFILE"
	at -M now + 24 hours <<<"$0 -r $CF_RESPONSE" >/dev/null 2>&1	#queue removal job
else										#sanity check. remove all and start over if too many records
	echo -e "$(date +%c) - $record_num individual DKIM records found. Purging all..." >>"$CFDYN_LOGFILE"
	for (( i=0;i<$record_num;i++ )); do
		purge_dkim_key "$(echo "$CF_RESPONSE" | head -$((i+1)) | tail -1)"
		remove_record "$(echo "$CF_RESPONSE" | head -$((i+1)) | tail -1)"
	done
	purge_atd_jobs "$(at -l | awk '{print $1;}')"		#purging only atd jobs that were created from this script, wherever it is placed
	add_record ; reload_mail_services	
fi
exit_script
