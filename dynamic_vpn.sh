#!/bin/bash

if ! [[ $(id -u) = 0 ]]; then
	echo -e "\033[1;31mERROR! Dynamic VPN script must be called as root\033[0m"
	exit 1
fi

#User-definable variables:	THESE MUST BE DEFINED BEFORE EXECUTION!
#conf_dir : Full path to folder containing VPN configuration files to pick from
#auth_file : Full path to file containing VPN account credentials. File path is written directly into configuration files, if not already existent.
#	FORMAT: (OpenVPN will handle username and password validity. This script only passes this data by reference.)
#
#	username
#	password
#
#	i.e. identical format used by OpenVPN 'auth-user-pass' directive
#
#
#logfile : Full path to logfile
#openvpn_unit : SystemD unit file for service. Also used for configuration symlink
#ssl_cn : Common Name for SSL CERT
#ssl_keyout : Full path to ssl key. Created by openssl
#ssl_certout : Full path to ssl cert.  Created by openssl
#p2p_autofs_mount : Optional handle for autofs. Pings to awaken before transmission-daemon, nginx started

conf_dir=null
auth_file=null
logfile=/dev/null
openvpn_unit=null
ssl_cn=null
ssl_keyout=null
ssl_certout=null
p2p_autofs_mount=null

#get server config randomly. remove old symlink and add new
get_config() {

	num=$(ls -1 "$conf_dir/"*.ovpn | wc -l)
	rand=$((1 + RANDOM % num))
	r_conf="$(ls -1 "$conf_dir/"*.ovpn | head -$rand | tail -1)"

	#unlink if already exists
	if [ -L "/etc/openvpn/$openvpn_unit.conf" ]; then
		unlink "/etc/openvpn/$openvpn_unit.conf"
	fi

	ln -s "$r_conf" "/etc/openvpn/$openvpn_unit.conf"

}

#remove auth-user-pass directive if it already exists, then add using global 'auth_file' variable
add_authfile() {

	fullpath="$(readlink "/etc/openvpn/$openvpn_unit.conf")"
	if [ $(grep -qF "auth-user-pass" "$fullpath") ]; then
		sed -i '/auth-user-pass/d' "$fullpath"
	fi

	sed -i "/<ca>/i auth-user-pass $auth_file" "$fullpath"
}

make_new_ssl() {

	echo -e "$(date +%c) --" >>"$logfile"
	openssl req -x509 -newkey rsa:4096 -keyout "$ssl_keyout" -out "$ssl_certout" -days 30 -nodes -subj "/CN=$ssl_cn" >>"$logfile" 2>&1

}

echo -e "$(date +%c) -- START ================================" >>"$logfile"

if [[ "$(systemctl is-active transmission-daemon.service)" == "active" ]]; then
	systemctl stop transmission-daemon.service
fi
if [[ "$(systemctl is-enabled transmission-daemon.service)" == "enabled" ]]; then
	systemctl disable transmission-daemon.service
fi
if [[ "$(systemctl is-active nginx.service)" == "active" ]]; then
	systemctl stop nginx.service
fi
if [[ "$(systemctl is-enabled nginx.service)" == "enabled" ]]; then
	systemctl disable nginx.service
fi

ret=-1
tries=0
while [ $ret = -1 ]; do
	
	#stop/disable existing openvpn client service
	if [[ "$(systemctl is-active "openvpn@$openvpn_unit.service")" == "active" ]]; then
		systemctl stop "openvpn@$openvpn_unit.service"
	fi

	get_config
	add_authfile
	systemctl start "openvpn@$openvpn_unit.service" && sleep 30
	tries=$((tries+1))
	tun_up="$(ip a | grep tun[0-9])"
	if [[ -z $tun_up ]]; then 
		continue
	else
		echo -e "$(date +%c) - Connected to: $(readlink "/etc/openvpn/$openvpn_unit.conf") after $tries tries. Public IP : $(curl -s -4 icanhazip.com)" >>"$logfile"
		ret=0
	fi		
done

#ping autofs mount. restart transmission and nginx
touch "$p2p_autofs_mount" >/dev/null 2>&1
make_new_ssl

for (( i=0;i<10;i++ )); do

	if [[ "$(systemctl is-active transmission-daemon.service)" == "inactive" ]]; then
		systemctl start transmission-daemon.service
	fi
	if [[ "$(systemctl is-active nginx.service)" == "inactive" ]]; then
		systemctl start nginx.service
	fi

	echo -e "$(date +%c) -- I$((i+1))\t:\tTRANSMISSION-DAEMON:$(systemctl is-active transmission-daemon.service)\tNGINX:$(systemctl is-active nginx.service)" >>"$logfile"

	if [[ "$(systemctl is-active transmission-daemon.service)" == "active" ]] && [[ "$(systemctl is-active nginx.service)" == "active" ]]; then
		break
	fi
	sleep 10
done

echo -e "$(date +%c) -- STOP =================================" >>"$logfile"
