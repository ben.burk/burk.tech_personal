#!/bin/bash

#DYNAMIC CLOUDFLARE DNS UPDATER
#Written against API v4.

#AUTHOR: Ben Burk
#DATE: 2019.08

#REQUIREMENTS
#jq
#curl
#grep
#ping
#sed
#cut
#printf

#ensure script is called as root
if ! [ $(id -u) = 0 ]; then
	echo -e "\033[31;1mERROR! Cloudflare Dynamic DNS script started without root permissions!\033[0m"
	exit 1
fi 

#INCLUSION 2019.09: getparams. Addition to allow fields from API file as strings surrounded by single quotes. Legacy support as well for strings without.
#INCLUSION 2019.10: valid_ip and network_status. Addition for additional sanity checks and logging purposes.
#INCLUSION 2019.10: getparams. Addition to allow fields from API file as strings surrounded by double quotes. Legacy support as well for strings without.

#User-definable variables:	THESE MUST BE DEFINED BEFORE EXECUTION!
#CF_FILE: Full path to file containing cloudflare API data
#	FORMAT:
#
#	CF_Key="API_KEY", 'API_KEY', or API_KEY		<--- YOUR CF API KEY goes here
#	CF_Email="API_KEY", 'API_EMAIL', or API_EMAIL	<--- YOUR CF API EMAIL goes here
#
#CF_RECORD_NAME : DNS record key, i.e. name of record
#CF_RECORD_TYPE : DNS record type, {A, CNAME, etc}
#CFDYN_LOGFILE : Full path to logfile

CF_API_KEY=null
CF_API_EMAIL=null
CF_FILE=null
CF_ID=null
CF_RECORD_ID=null
CF_RESPONSE=null
CF_ADD_SUCCESS=null
CF_RECORD_NAME=null
CF_RECORD_TYPE=null
CURRENT_IP=null
CFDYN_LOGFILE=/dev/null		#set this to /dev/null for no output, or /dev/stdout to STDOUT

if [ ! -f $CFDYN_LOGFILE ]; then
	touch $CFDYN_LOGFILE
fi

valid_ip() {

	if [[ -z $1 ]]; then return 1 ; fi

    	local  ip=$1
    	local  stat=1

    	if [[ $ip =~ ^[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}$ ]]; then
        	OIFS=$IFS
        	IFS='.'
        	ip=($ip)
        	IFS=$OIFS
       		[[ ${ip[0]} -le 255 && ${ip[1]} -le 255 \
            	&& ${ip[2]} -le 255 && ${ip[3]} -le 255 ]]
        	stat=$?
    	fi
    	return $stat

}

network_status() {

	ping -c 1 8.8.8.8 &>/dev/null
	local phys=$?
	if ! [ $phys = 0 ]; then echo -e "$(date +%c) ::FATAL - No route to internet. Ping error returned for 8.8.8.8 (google dns)" >>$CFDYN_LOGFILE ; return 1 ; fi
	ping -c 1 "$1" &>/dev/null
	local dns=$?
	if ! [ $dns = 0 ]; then echo -e "$(date +%c) ::FATAL - DNS lookup failed. Ping error returned for $1" >>$CFDYN_LOGFILE ; return 1 ; fi
	return 0

}

get_params() {

	if [ ! -f $CF_FILE ]; then
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE does not exist!" >>$CFDYN_LOGFILE
		exit 1
	fi

	grep -q "^CF_Key=" $CF_FILE &>/dev/null
        cf_key_chk=$?
	if [[ $cf_key_chk -eq 1 ]]; then	
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE missing CF_Key!" >>$CFDYN_LOGFILE
		exit 1
	else
		CF_API_KEY="$(grep "CF_Key" $CF_FILE | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" #permit field in file be surrounded by single, double quotes
	fi
	grep -q "^CF_Email=" $CF_FILE &>/dev/null
	cf_email_chk=$?
	if [[ $cf_email_chk -eq 1 ]]; then
		echo -e "$(date +%c) ::FATAL - Credentials file $CF_FILE missing CF_Email!" >>$CFDYN_LOGFILE
		exit 1
	else
		CF_API_EMAIL="$(grep "CF_Email" $CF_FILE | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" #permit field in file be surrounded by single, double quotes
	fi

}

get_domain_id() {

	CF_ID="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones" \
		     -H "Content-Type:application/json" \
		     	          -H "X-Auth-Key:${CF_API_KEY}" \
				  -H "X-Auth-Email:${CF_API_EMAIL}" | jq -r '.result[].id // empty')"

}

check_existing_record() {

	CF_RESPONSE="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/${CF_ID}/dns_records?type=${CF_RECORD_TYPE}&name=${CF_RECORD_NAME}&page=1&per_page=20&order=type&direction=desc&match=all" \
			     -H "Content-Type:application/json" \
			     	          -H "X-Auth-Key:${CF_API_KEY}" \
					  -H "X-Auth-Email:${CF_API_EMAIL}" | jq -r '.result[].content // empty')"

}

update_record() {

	#grab cloudflare record id
	CF_RECORD_ID="$(curl -s -X GET "https://api.cloudflare.com/client/v4/zones/${CF_ID}/dns_records?type=${CF_RECORD_TYPE}&name=${CF_RECORD_NAME}&page=1&per_page=20&order=type&direction=desc&match=all" \
		     -H "X-Auth-Email: ${CF_API_EMAIL}" \
		     -H "X-Auth-Key: ${CF_API_KEY}" \
		     -H "Content-Type: application/json" | jq -r '.result[].id')"

	#update existing record using obtained id and print
	curl -s -X PUT "https://api.cloudflare.com/client/v4/zones/${CF_ID}/dns_records/${CF_RECORD_ID}" \
		     -H "X-Auth-Email: ${CF_API_EMAIL}" \
		          -H "X-Auth-Key: ${CF_API_KEY}" \
			       -H "Content-Type: application/json" \
			       --data "{\"type\":\"${CF_RECORD_TYPE}\",\"name\":\"${CF_RECORD_NAME}\",\"content\":\"${CURRENT_IP}\",\"ttl\":1,\"proxied\":false}" \
			       | jq -r '.' >>$CFDYN_LOGFILE

}

add_record() {

	#add new record and print
	CF_ADD_SUCCESS="$(curl -s -X POST "https://api.cloudflare.com/client/v4/zones/${CF_ID}/dns_records" \
	   -H "X-Auth-Email: ${CF_API_EMAIL}" \
	   -H "X-Auth-Key: ${CF_API_KEY}" \
	   -H "Content-Type: application/json" \
	   --data "{\"type\":\"${CF_RECORD_TYPE}\",\"name\":\"${CF_RECORD_NAME}\",\"content\":\"${CURRENT_IP}\",\"ttl\":1,\"priority\":10,\"proxied\":false}" \
	   | jq -r '.' | tee -a $CFDYN_LOGFILE | jq -r '.success')"

}


#grab api key and email from file
get_params

#grab domain id
get_domain_id

if [[ -z "$CF_ID" ]]; then 
	echo -e "$(date +%c) ::FATAL - Invalid zone id '${CF_ID}' received from CF" >>$CFDYN_LOGFILE
	if ! network_status api.cloudflare.com; then exit 1 ; fi
	echo -e "$(date +%c) - No reported error with inet route or dns. Assuming internal CF issue. No change" >>$CFDYN_LOGFILE ; exit 1
fi

#get current public ip address and make API query.
#2020.04 Removed use of 3rd party API initially. Only used if system is behind NAT

CURRENT_IP="$(hostname -I | tr ' ' '\n' | grep -Ev '^(10|127|169\.254|172\.1[6-9]|172\.2[0-9]|172\.3[0-1]|192\.168)\.' | tr -d '\n')"
if ! valid_ip "$CURRENT_IP"; then CURRENT_IP="$(curl -s -4 icanhazip.com)" ; fi		#system likely behind NAT so use 3rd party
if ! valid_ip "$CURRENT_IP"; then echo -e "$(date +%c) ::FATAL - Invalid IP address '${CURRENT_IP}' received from 3rd party" >>$CFDYN_LOGFILE ; if ! network_status icanhazip.com; then exit 1 ; fi ; echo -e "$(date +%c) - No reported error with inet route or dns. Assuming internal 3rd party issue. No change" >>$CFDYN_LOGFILE ; exit 1 ; fi			#final integrity check for string returned from 3rd party. If invalid, exit without change

#query cloudflare for existing record.
check_existing_record

#Operational check. Adds record if does not exist. Updates if value is different. Exits if record value matches.
if [ -z "$CF_RESPONSE" ]; then
	if ! network_status api.cloudflare.com; then exit 1 ; fi
	echo -e "$(date +%c) - No ${CF_RECORD_TYPE} record could be found for '${CF_RECORD_NAME}'...\n" >>$CFDYN_LOGFILE
	add_record ; printf '\n' >>$CFDYN_LOGFILE
	if [[ "$(echo $CF_ADD_SUCCESS | awk '{ print tolower($0) }')" == "true" ]]; then echo -e "$(date +%c) - Successfully added DNS record for '${CF_RECORD_NAME}'..." >>$CFDYN_LOGFILE ; else echo -e "$(date +%c) - Failed to add DNS record for '${CF_RECORD_NAME}'..." >>$CFDYN_LOGFILE ; fi
else
	if ! valid_ip "$CF_RESPONSE"; then echo "$(date +%c) - Invalid address! Found ${CF_RECORD_TYPE} record with '${CF_RESPONSE}'. Updating...\n" >>$CFDYN_LOGFILE ; update_record ; printf '\n' >>$CFDYN_LOGFILE
	else
		if [[ "$CF_RESPONSE" != "$CURRENT_IP" ]]; then
			echo -e "$(date +%c) - Address changed! Found ${CF_RECORD_TYPE} record with '${CF_RESPONSE}'. Current IP is '${CURRENT_IP}'. Updating...\n" >>$CFDYN_LOGFILE
			update_record ; printf '\n' >>$CFDYN_LOGFILE
		else
			echo "$(date +%c) - No change necessary! Current IP '${CURRENT_IP}' matches ${CF_RECORD_TYPE} record for '${CF_RECORD_NAME}' with '${CF_RESPONSE}'" >>$CFDYN_LOGFILE
		fi
	fi
fi
