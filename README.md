Custom scripts written for various projects. All licensed under MIT.

	batch_mailer.sh		:	Attachment Batch Emailer. Takes a file list as input and splits amongst emails per predetermined variable.
	cf_dynamicdkim.sh	:	Dynamic DKIM Updater. Built around CloudFlare API v4.
	cf_dynamicdns.sh    	:    	Dynamic DNS Updater. Built around CloudFlare API v4.
	crypt_maildir.sh        :	Encryption/decryption tool for mail handled by dovecot's mail_crypt plugin.
	dynamic_vpn.sh		:	Dynamic VPN Cycler. Randomly picks VPN and handles NGINX proxy key cycling.
	fail2ban_sort.sh    	:    	Custom sorting script for a Fail2ban setup with a flat file as the blacklist db.
	immuniweb_sslreport.sh	:	SSL Report Tool. Queues Immuniweb SSL report generation and emails generated pdfs.
	maildirincbackup.sh 	:	Incremental backup utility for MAILDIR style mail filesystems.
