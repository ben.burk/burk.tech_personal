#!/usr/bin/python3
"""
crypowatch.py

This is a command-line script to retrieve cryptocurrency price/trend data and email profit/roi data calculated from provided investment data.

Usage:
    cryptowatch.py [options]
    cryptowatch.py (-h | --help)
    cryptowatch.py --version

Options:
    -h --help               Show this screen
    --version               Show version
    --conf=<conf>        Path to conf
    --tickers=<tickers>  Override tickers from default list in config file with comma-separated list
    --infile=<file>      Path to file used to calculate profit/roi
    --recipient=<email>  Specify recipient email address for generated crypto report
    --infura=<project_id>   Specify infura project id. Ethereum blockchain entry point
"""

import docopt
import requests
import json
import smtplib, ssl
import sys
if (sys.version_info > (3, 0)):
    from configparser import ConfigParser
else:
    from ConfigParser import ConfigParser
from os import path
from datetime import datetime
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
from decimal import Decimal
from web3 import Web3
from bs4 import BeautifulSoup

####################################################################-------------- HELPER REGION -------------##############################################################################


dictfilt = lambda x,y: dict([ (i,x[i]) for i in x if i in set(y) ])
def listfilt(id_x_pairs, wanted_key): return [id_x_pairs[x][wanted_key] for x in range(0, len(id_x_pairs))]


def sort_result_by_key(id_x_pairs, ordered_keys, wanted_key):
    resultant = []
    if len(id_x_pairs) != len(ordered_keys): return None
    if not any(wanted_key in x for x in id_x_pairs): return None
    for x in range(0, len(ordered_keys)):
        resultant.append(next((item for item in id_x_pairs if item[wanted_key] == ordered_keys[x]), None))
    return resultant


#TODO: have key_order_tuple be pair of dictionaries specifying which input to search for what key
def join_results_on_key(id_x_pairs, id_y_pairs, wanted_key, key_order_tuple):
    resultant = []
    if len(key_order_tuple) != 2: return None
    if len(id_x_pairs) != len(id_y_pairs): return None
    for x in range(0, len(key_order_tuple)):
        if not any(key_order_tuple[x] in y for y in id_x_pairs) and not any(key_order_tuple[x] in y for y in id_y_pairs): return None
    for x in range(0, len(id_x_pairs)):
        temp_d = {}
        temp_d[key_order_tuple[0]] = next((item[key_order_tuple[0]] for item in id_x_pairs if item[wanted_key] == (id_x_pairs[x])[wanted_key]), None)
        temp_d[key_order_tuple[1]] = next((item[key_order_tuple[1]] for item in id_y_pairs if item[wanted_key] == (id_y_pairs[x])[wanted_key]), None)
        resultant.append(temp_d)
    return resultant


def format_for_email(data, exchange):
    tokens = "" 
    investments = ""
    percent_total = ""
    totals = ""

    for k1 in data['coins'].keys():
        tokens+="{0}:\t\t{1}\n".format(k1.upper(), round(Decimal(data['coins'][k1]['token_price']), 4))
        if data['coins'][k1]['amount'] > 0 or k1.upper() == 'MNE':
            investments+="{0}\n".format(k1.upper())
            if k1.upper() == 'MNE':
                investments+="Mined:\t\t{0}\n".format(round(Decimal(data['coins'][k1]['amount']), 4))
            if k1.upper() == 'ADA':
                investments+="Rewards:\t{0}\nRewards Value:\t{1}\n".format(round(Decimal(data['coins'][k1]['stake']['total_rewards']), 4), round(Decimal(data['coins'][k1]['stake']['total_rewards'] * data['coins'][k1]['token_price']), 4))
            investments+="Value:\t\t{0}\nProfit:\t\t{1}\nROI(%):\t\t{2}\n\n".format(round(Decimal(data['coins'][k1]['value']), 2), (round(Decimal(data['coins'][k1]['profit']), 2) if data['coins'][k1]['profit'] is not None else "N/A"), (round(Decimal(data['coins'][k1]['roi']), 2) if data['coins'][k1]['roi'] is not None else "N/A"))
        if data['coins'][k1]['amount'] > 0 or k1.upper() == 'MNE':
            percent_total+="{0}:\t\t{1}%\n".format(k1.upper(), round(Decimal((data['coins'][k1]['value'] / data['totals']['value']) * 100.0), 2))

    totals="Value:\t\t{0}\nProfit:\t\t{1}\nROI(%):\t\t{2}".format(round(Decimal(data['totals']['value']), 2), round(Decimal(data['totals']['profit']), 2), round(Decimal(data['totals']['roi']), 2))

    return """\
Reference: {0}


TOKENS:
{1}

INVESTMENTS:

{2}

PERCENT OF TOTAL BY TOKEN:
{3}

TOTAL INVESTMENT:
{4}""".format(exchange, tokens, investments, percent_total, totals)


####################################################################------------ END HELPER REGION ------------#############################################################################
####################################################################----------- MARKETPLACE REGION ------------#############################################################################


def coinmarketcap_get_coin_ids_by_tickers(api_key, tickers):
    joined_args = ','.join(tickers).lower()
    payload = {'symbol': joined_args}
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/map'
    headers = {'X-CMC_PRO_API_KEY': api_key, 'Accept': 'application/json'}
    try:
        r = requests.get(url, headers=headers, params=payload)
    except Exception as e:
        raise Exception("ERROR! {0}".format(repr(e)))
    try:
        json_resp = json.loads(r.text) ; resultant = []
    except json.JSONDecodeError as jde:
        raise Exception("ERROR! Response received from CoinMarketCap is not valid JSON. {0}".format(repr(jde)))
    if json_resp['status']['error_code'] != 0:
        raise Exception("ERROR! HTTP {0} : {1}. Unable to fetch information from CoinMarketCap".format(json_resp['status']['error_code'], json_resp['status']['error_message']))
    for k1 in json_resp['data']:
        wanted_keys = ("id", "symbol")
        resultant.append(dictfilt(k1, wanted_keys))
    return resultant


def coinmarketcap_get_coin_prices_by_ids(api_key, ids):
    joined_args = ','.join(map(str, ids))
    payload = {'id': joined_args}
    url = 'https://pro-api.coinmarketcap.com/v1/cryptocurrency/quotes/latest'
    headers = {'X-CMC_PRO_API_KEY': api_key, 'Accept': 'application/json'}
    try:
        r = requests.get(url, headers=headers, params=payload)
    except Exception as e:
        raise Exception("ERROR! {0}".format(repr(e)))
    try:
        json_resp = json.loads(r.text) ; resultant = {}
    except json.JSONDecodeError as jde:
        raise Exception("ERROR! Response received from CoinMarketCap is not valid JSON. {0}".format(repr(jde)))
    if json_resp['status']['error_code'] != 0:
        raise Exception("ERROR! HTTP {0} : {1}. Unable to fetch information from CoinMarketCap".format(json_resp['status']['error_code'], json_resp['status']['error_message']))
    for x in ids:
            resultant[str(x)] = {}
            json_resp['data'][str(x)]['price'] = json_resp['data'][str(x)]['quote']['USD']['price']
            wanted_keys1 = ("id", "symbol", "price") ; wanted_keys2 = ("id", "symbol", "quote")
            resultant[str(x)]['basic'] = dictfilt(json_resp['data'][str(x)], wanted_keys1)
            resultant[str(x)]['detail'] = dictfilt(json_resp['data'][str(x)], wanted_keys2)
    return resultant


def coinmarketcap_get_basic_resultant(api_key, tickers, ticker_id_ignores):
    ignores_dict = {n.split(':')[0]:n.split(':')[1] for n in ticker_id_ignores}
    id_ignores = list(ignores_dict.keys())
    id_sym_pairs = coinmarketcap_get_coin_ids_by_tickers(api_key, tickers)
    ids = listfilt(id_sym_pairs, 'id')
    for x in ids:
        if str(x) in id_ignores and next((item["symbol"].lower() for item in id_sym_pairs if item["id"] == x), None) == ignores_dict[str(x)]:
                ids.remove(x) ; id_sym_pairs = [i for i in id_sym_pairs if not (i['id'] == x)]
    id_quote_pairs_all = coinmarketcap_get_coin_prices_by_ids(api_key, ids)
    id_quote_pairs = []
    for x in ids:
            id_quote_pairs.append(dictfilt(id_quote_pairs_all[str(x)]['basic'], ("id", "price")))
    return sort_result_by_key(join_results_on_key(id_sym_pairs, id_quote_pairs, 'id', ("symbol", "price")), [x.upper() for x in tickers], 'symbol')

####################################################################----------- END MARKETPLACE REGION --------#############################################################################
####################################################################-------------- EXTRAS REGION --------------#############################################################################


#TODO: add error checking for response from infura and exception catching for requests made
#if MNE token is traded/moved, etc
#increase the origin_mining_block_number in the source json
#i.e. if Available Balance = 12415.056 and origin_mining_block_number = 1891651
#12415.056 / 0.00032 = 38797050
#38797050 + 1891651 = 40688701
#origin_mining_block_number, or 'mining_origin' in the source json should be changed to '40688701'
def calc_mne_rewards(infura_project_id, origin_mining_block_number):
    w3 = Web3(Web3.HTTPProvider('https://mainnet.infura.io/v3/{0}'.format(infura_project_id)))
    curr_block = w3.eth.blockNumber
    initial_block = 3516521         #initial minereum block on ethereum
    delta_ref = curr_block - initial_block - origin_mining_block_number
    return delta_ref * 0.00032


def fetch_ada_rewards(stake_addr):
    #pooltool.io deprecated. service moved from graphql to webpack format. switched to cardanoscan.io - 03.03.2021
    #payload = {"operationName":"MyQuery","variables":{"address":"{0}".format(stake_addr)},"query":"query MyQuery($address: String = \"\") {\n  pt_addresses_history(where: {stake_address: {_eq: $address}}) {\n    epoch\n    stake_address\n    poolid\n    stake\n    stake_rewards\n  }\n}\n"}
    #url = 'https://gql2.pooltool.io/v1/graphql'
    
    #try:
        #r = requests.post(url, headers=headers, json=payload)
    #except Exception as e:
        #raise Exception("ERROR! {0}".format(repr(e)))
    #try:
        #json_resp=json.loads(r.text)
    #except:
        #raise Exception("ERROR! Response received from PoolTool is not valid JSON. {0} \n\n{1}".format(repr(jde), r.text))
    #if "errors" in json_resp:
        #raise Exception("ERROR! Error response received from PoolTool. {0}".format(json_resp['errors']))
    #return json_resp
    
    url = 'https://cardanoscan.io/stakekey/{0}'.format(stake_addr)
    headers = {'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8'}
    try:
        r = requests.get(url, headers=headers)
    except Exception as e:
        raise Exception("ERROR! {0}".format(repr(e)))
    try:
        soup = BeautifulSoup(r.text, 'html.parser')
    except Exception as e:
        raise Exception("ERROR! Response received from CardanoScan is not valid HTML! {0} \n\n{1}".format(repr(e), r.text))
    try:
        ada_rewards = "{0}{1}".format(soup.find("span", string="Rewards Available").find_next_sibling("div").find("span").text, soup.find("span", string="Rewards Available").find_next_sibling("div").find("span").find_next_sibling("span").text)
    except Exception as e:
        raise Exception("ERROR! Unable to search HTML! {0} \n\nDEBUG:\nsoup.find('span', string='Rewards Available') = {1}\nsoup.find('span', string='Rewards Available').find_next_sibling('div') = {2}\nsoup.find('span', string='Rewards Available').find_next_sibling('div').find('span') = {3}\nsoup.find('span', string='Rewards Available').find_next_sibling('div').find('span').find_next_sibling('span') = {4}".format(repr(e), soup.find("span", string="Rewards Available"), soup.find("span", string="Rewards Available").find_next_sibling("div"), soup.find("span", string="Rewards Available").find_next_sibling("div").find("span"), soup.find("span", string="Rewards Available").find_next_sibling("div").find("span").find_next_sibling("span")))
    return ada_rewards


####################################################################------------ END EXTRAS REGION ------------#############################################################################
####################################################################--------------- EMAIL REGION --------------#############################################################################


def send_email_report(smtp_server, user, password, sender_email, recipient_email, report_message, attachment):
    context = ssl.create_default_context()
    msg = MIMEMultipart()
    msg['Subject'] = "CRYPTOWATCH: {0}".format(datetime.today().strftime('%Y-%m-%d %I%p'))
    msg['From'] = sender_email
    msg['To'] = recipient_email
    msg.attach(MIMEText(report_message))
    part = MIMEBase('application', 'json')
    part.set_payload(open(attachment, 'rb').read())
    encoders.encode_base64(part)
    part.add_header('Content-Disposition', 'attachment; filename="%s"' % path.basename(attachment))
    msg.attach(part)
    try:
        server = smtplib.SMTP(smtp_server, 587)
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(user, password)
        server.send_message(msg)
    except Exception as e:
        print(e) ; sys.exit(1)
    finally:
        server.quit()

def send_debug_report(smtp_server, user, password, sender_email, recipient_email, report_message):
    context = ssl.create_default_context()
    msg = MIMEMultipart()
    msg['Subject'] = "CRYPTOWATCH [DEBUG]: {0}".format(datetime.today().strftime('%Y-%m-%d %I%p'))
    msg['From'] = sender_email
    msg['To'] = recipient_email
    msg.attach(MIMEText("Fatal error occured. Coinmarketcap might be doing something fucky with their API. If there are any duplicate id,symbol pairs here, there must be an ignore set for one of the ids. Tickers returned from request as follows:", 'plain', 'utf-8'))
    msg.attach(MIMEText(report_message))
    try:
        server = smtplib.SMTP(smtp_server, 587)
        server.ehlo()
        server.starttls(context=context)
        server.ehlo()
        server.login(user, password)
        server.send_message(msg)
    except Exception as e:
        print(e) ; sys.exit(1)
    finally:
        server.quit()
        

####################################################################------------ END EMAIL REGION -------------#############################################################################
####################################################################-------------- MATH REGION ----------------#############################################################################

def do_math_with_price_data(price_data, infura_project_id, infile):
    if not path.exists(infile):
        print("ERROR! Input file {0} does not exist".format(infile)) ; sys.exit(1)
    with open(infile) as f:
        try:
            data = json.load(f)
        except json.JSONDecodeError as jde:
            raise Exception("ERROR! {0}".format(repr(jde)))
        for k1 in data['coins'].keys():
            for x in range(0, len(price_data)):
                if price_data[x]['symbol'] == k1.upper():
                    if k1.upper() == 'MNE':
                        data['coins'][k1]['amount'] = calc_mne_rewards(infura_project_id, data['coins'][k1]['mining_origin'])
                    if k1.upper() == 'ADA':
                        if data['coins'][k1]['stake']['staking'] == True:
                            for y in range(0, len(data['coins'][k1]['stake']['stake_key'])):
                                ada_quer = float(fetch_ada_rewards(data['coins'][k1]['stake']['stake_key'][y]))
                                data['coins'][k1]['stake']['total_rewards'] += ada_quer
                                #pooltool.io deprecated. switched to cardanoscan.io. service does not provide history - 03.03.2021
                                #ada_quer_hist = sorted(ada_quer['data']['pt_addresses_history'], key = lambda i: i['epoch'], reverse=True)
                                #data['coins'][k1]['stake']['history'][data['coins'][k1]['stake']['stake_key'][y]] = []
                                #for z in range(0, len(ada_quer_hist)):
                                    #ada_quer_hist[z]['stake'] /= 1000000
                                    #ada_quer_hist[z]['stake_rewards'] /= 1000000
                                    #if ada_quer_hist[z]['epoch'] > ada_quer_hist[0]['epoch'] - 5:
                                        #data['coins'][k1]['stake']['history'][data['coins'][k1]['stake']['stake_key'][y]].append(ada_quer_hist[z])
                                    #data['coins'][k1]['stake']['total_rewards'] += ada_quer_hist[z]['stake_rewards']
                                del data['coins'][k1]['stake']['stake_key'][y]
                            data['coins'][k1]['amount'] += data['coins'][k1]['stake']['total_rewards']
                    data['coins'][k1]['token_price'] = price_data[x]['price']
                    data['coins'][k1]['value'] = price_data[x]['price'] * data['coins'][k1]['amount']
                    if data['coins'][k1]['profit'] is not None:
                        data['coins'][k1]['profit'] = data['coins'][k1]['value'] - (data['coins'][k1]['cost'] + (0 if data['coins'][k1]['swing'] == None else data['coins'][k1]['swing']))
                    if data['coins'][k1]['roi'] is not None:
                        data['coins'][k1]['roi'] = (data['coins'][k1]['profit'] / ((data['coins'][k1]['cost']) + (0 if data['coins'][k1]['swing'] == None else data['coins'][k1]['swing']))) * 100.0
                    data['totals']['value'] += data['coins'][k1]['value']
                    data['totals']['cost'] += data['coins'][k1]['cost']
        data['totals']['cost'] += (0 if data['totals']['swing_offset'] == None else data['totals']['swing_offset'])
        data['totals']['profit'] = data['totals']['value'] - data['totals']['cost']
        data['totals']['roi'] = (data['totals']['profit'] / data['totals']['cost']) * 100.0
        with open(path.splitext(infile)[0]+'.json', 'w') as f2:
            json.dump(data, f2)
        return data


####################################################################------------- END MATH REGION -------------#############################################################################



def main(args):

    exchange = None
    email_status = None
    tickers = None
    ticker_id_ignores = None
    api_key = None
    infura_project_id = None
    server = None
    username = None
    password = None
    return_addr = None
    input_file = None
    recipient = None
    debug_addr = None

    def get_master_conf(filename):
        config = ConfigParser()
        config.read(filename)
        items = dict(config.items('master'))
        return items['exchange'], items['email_status'], items['default_tickers'], items['ticker_id_ignores']
    def get_coinmarketcap_conf(filename):
        config = ConfigParser()
        config.read(filename)
        items = dict(config.items('coinmarketcap'))
        return items['apikey']
    def get_email_conf(filename):
        config = ConfigParser()
        config.read(filename)
        items = dict(config.items('email'))
        return items['server'], items['username'], items['password'], items['return_addr'], items['debug_addr']
    def get_infura_conf(filename):
        config = ConfigParser()
        config.read(filename)
        items = dict(config.items('infura'))
        return items['project_id']
   
    if args['--conf'] is None:
        print("ERROR: You must specify a conf file.  i.e. --conf filename")
    else:
        exchange, email_status, tick, tid_ign = get_master_conf(args['--conf'])
        try:
            tickers = tick.rstrip(',').split(',')
            ticker_id_ignores = tid_ign.rstrip(',').split(',')
        except ValueError as ve:
            print(ve) ; sys.exit(1)
        if exchange == 'CoinMarketCap':
            api_key = get_coinmarketcap_conf(args['--conf'])
        if email_status == 'True' or email_status == 'true':
            server, username, password, return_addr, debug_addr = get_email_conf(args['--conf'])
        infura_project_id = get_infura_conf(args['--conf'])

    if args['--tickers'] is not None:
        tickers = args['--tickers'].rstrip(',').split(',')
    if args['--infura'] is not None:
        infura_project_id = args['--infura']

    if exchange == 'CoinMarketCap':
        if tickers is None or not isinstance(tickers, list):
            print("ERROR: Must specify default list of tickers in config file if --tickers override not used") ; sys.exit(1)
        resultant_data = coinmarketcap_get_basic_resultant(api_key, tickers, ticker_id_ignores)
        if resultant_data is None:
            debug_report = coinmarketcap_get_coin_ids_by_tickers(api_key, tickers)
            send_debug_report(server, username, password, return_addr, debug_addr, json.dumps(debug_report))
            exit()
        if args['--infile'] is None:
            print(resultant_data) ; sys.exit(0)
        input_file = args['--infile']
        adj_data = do_math_with_price_data(resultant_data, infura_project_id, input_file)
        if args['--recipient'] is not None and (email_status == 'True' or email_status == 'true'):
            recipient = args['--recipient']
            if path.dirname(input_file) == "":
                with open('investment.json', 'w') as f:
                    json.dump(adj_data, f)
                send_email_report(server, username, password, return_addr, recipient, format_for_email(adj_data, exchange), 'investment.json')
            else:
                with open('{0}/investment.json'.format(path.dirname(input_file)), 'w') as f:
                    json.dump(adj_data, f)
                send_email_report(server, username, password, return_addr, recipient, format_for_email(adj_data, exchange), '{0}/investment.json'.format(path.dirname(input_file)))

if __name__ == "__main__":
    version = __doc__.strip().split("\n")[0]
    args = docopt.docopt(__doc__, version=version)
    status = main(args)
    sys.exit(status)
