#!/bin/bash

MAIL_ROOT=/mail/burk.tech
META_DIR="${MAIL_ROOT%\/*}/.incbackup"
LOGFILE=/var/log/maildirincbackup/maildirincbackup.log
CRONFILE=/var/spool/cron/crontabs/incbackup
STARTED="${META_DIR}/.started"
STOPPED="${META_DIR}/.stopped"
LOCKFILE="${META_DIR}/.lock"
PIDFILE=/var/lib/maildirincbackup/maildirincbackup.pid		#placed here so I don't have to recreate at every boot. /run is tmpfs
FULL_LIST="${META_DIR}/.full_filelist"
PREV_FULL_LIST="${META_DIR}/.prev_full_filelist"
FULL_DICT="${META_DIR}/.full_dict.json"
PREV_FULL_DICT="${META_DIR}/.prev_full_dict.json"
INC_LIST="${META_DIR}/.inc_filelist"
PREV_INC_LIST="${META_DIR}/.prev_inc_filelist"
INC_DICT="${META_DIR}/.inc_dict.json"
PREV_INC_DICT="${META_DIR}/.prev_inc_dict.json"
TIMELINE="${META_DIR}/.timeline.json"
PREV_TIMELINE="${META_DIR}/.prev_timeline.json"
IGNORE_LIST="${META_DIR}/.ignore_list"
EPOCH_LIST="${META_DIR}/.epoch_list"
PUSH_LIST="${META_DIR}/.push_list"
GPG_ENABLED=1
GPG_RECIPIENT="smtparch.burk.tech"
REMOTE_PATH="/backups"
DEBUG=0
VIRTUAL_USERS_DB="mailserver"
BACKUP_VIRTUAL_USERS=1
BACKUP_VIRTUAL_USERS_OUTFILE="${META_DIR}/.virtual_users.mysql"
BACKUP_VIRTUAL_USERS_CMD="mysqldump --single-transaction ${VIRTUAL_USERS_DB} >$BACKUP_VIRTUAL_USERS_OUTFILE"

#~/.ssh/config REQUIRED to be configured for incbackup user i.e.
#Host fqdn.example.com
#	User incbackup
#	IdentityFile ~/.ssh/id_rsa
#	Port 22
#

tty -s ; IS_INTERACTIVE=$?			#0 means interactive

cleanup_lock() { if [ -f $LOCKFILE ]; then rm -f $LOCKFILE ; fi }
cleanup_pid() { if [ -f $PIDFILE ]; then rm -f $PIDFILE ; fi }
trap cleanup_pid 1 2 3 9 15	#catch SIGHUP, SIGINT, SIGQUIT, SIGKILL, SIGTERM. remove PID file before exit

#ARGUMENTS : A prefix string to classify the line(s) written. A variable specifying the lines to write.
#RETURNS : None
#i.e. 			write_to_log "NOTICE" "This is written" 	->	YYYY-MM-DD : hh:mm:ss :: NOTICE - This is written
write_to_log() {

	if [ ! -d $(dirname $LOGFILE) ]; then mkdir -p $(dirname $LOGFILE) 2>/dev/null ; if [ $? -ne 0 ]; then write_to_stderr "ERROR! Unable to create logfile. Unable to create directory $(dirname $LOGFILE)" ; exit ; fi ; fi
	echo -e "$(date '+%Y-%m-%d - %H:%M:%S') :: ${1} - ${2}" >>$LOGFILE 2>/dev/null ; if [ $? -ne 0 ]; then write_to_stderr "ERROR! Unable to create logfile. Unable to create file ${LOGFILE}" ; exit ; fi

}

write_to_stderr() { echo "$@" 1>&2; }

#ARGUMENTS : None
#RETURNS : None
create_lock() {

	if [ ! -d $(dirname $LOCKFILE) ]; then mkdir -p $(dirname $LOCKFILE) 2>/dev/null ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to create lockfile. Unable to create directory $(dirname $LOCKFILE)" ; exit_script ; fi ; fi
	touch $LOCKFILE 2>/dev/null ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to create lockfile. Unable to create file ${LOCKFILE}" ; exit_script ; fi
}

#ARGUMENTS : None
#RETURNS : None
create_pid() {

	if [ ! -d $(dirname $PIDFILE) ]; then mkdir -p $(dirname $PIDFILE) 2>/dev/null ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to create pidfile. Unable to create directory $(dirname $PIDFILE)" ; exit_script ; fi ; fi
	echo $$ >$PIDFILE 2>/dev/null ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to create pidfile. Unable to create file ${PIDFILE}" ; exit_script ; fi
}

#ARGUMENTS : A prefix string to classify the line written.
#RETURNS : None
start_script() {

	write_to_log "START" "${1} =============================================================="
}

exit_script() {

	write_to_log "STOP" "${1} =============================================================="	
	exit
}

#ARGUMENTS : A filesystem string specifying a file list
#RETURNS : Variable specifying the size in bytes of all files specified
calc_size() {

	t_size=0	
	if ! [ -f $1 ]; then write_to_log "ERROR" "File list ${1} passed to calc_size does not exist! Exiting..." ; exit_script ; fi
	if ! [ -s $1 ]; then write_to_log "ERROR" "File list ${1} passed to calc_size is empty! Returning size ${t_size} ..." ; echo $t_size ; exit_script ; fi
	while read LINE ; do
		if [[ $LINE == *".deleted" ]]; then continue; fi 	#addition for incremental list. If file ends in .deleted, ignore.
		if [ ! -f $LINE ]; then write_to_log "ERROR" "File list ${1} passed to calc_size has nonexistent file : ${LINE} . File was likely moved or deleted. Ignoring..." ; continue ; fi
		t=$(du -b $LINE | sed 's|\s.*$||')
		t_size=$((t_size+t))
	done < $1
	echo $t_size

}

#ARGUMENTS : A filesystem string specifying a file list
#RETURNS : A json dictionary, including last modification time and md5sum of each file
make_dict() {
	
	if [ ! -f $1 ]; then write_to_log "ERROR" "File list ${1} passed to make_dict does not exist! Exiting..." ; exit_script ; fi
	if [ ! -s $1 ]; then write_to_log "ERROR" "File list ${1} passed to make_dict is empty! Exiting..." ; exit_script ; fi
	ln=$(cat $1 | grep -v '.deleted$' | wc -l)
	tlist="/tmp/tlist_mdict" ; cat $1 | grep -v '.deleted$' >$tlist
	tdict="/tmp/tdict_mdict" ; echo "{}" >$tdict
	while read LINE ; do
		if [ ! -f $LINE ]; then write_to_log "ERROR" "File list ${1} passed to make_dict has nonexistent file : ${LINE} . File was likely moved or deleted. Ignoring..." ; continue ; fi
		hash="$(md5sum $LINE | sed 's|\ .*||')" ; mtime="$(stat -c %Y $LINE)"
		jq -r --arg f "${LINE}" --arg m "${mtime}" --arg n "${hash}" '. + {"\($f)": {"MTIME": $m, "MD5": $n}} | tostring' $tdict | sponge $tdict
	done < $tlist
	cat $tdict
	rm -f $tdict ; rm -f $tlist

}

#ARGUMENTS : None
#RETURNS : None
#stub function to simplify routines
make_full_dict() {

	if [ -f $FULL_DICT ]; then
		cat $FULL_DICT >$PREV_FULL_DICT
	fi
	
	make_dict $FULL_LIST >$FULL_DICT
}

#ARGUMENTS : None
#RETURNS : None
#stub function to simplify routines
make_inc_dict() {

	if [ -f $INC_DICT ]; then
		cat $INC_DICT >$PREV_INC_DICT
	fi
	
	make_dict $INC_LIST >$INC_DICT
}	

#ARGUMENTS : None
#RETURNS : None
show_help() {
	
	echo "$(echo $0 | sed 's!.*/!!; s!\..*!!') - Incremental Backup Utility for MAILDIR style mail filesystems."
	echo -e "Written by Ben Burk, copyright BURKTECH 2019. Licensed under the MIT free software license."
	echo -e "usage: ./$(echo $0 | sed 's!.*/!!') [--installcron] [--auto] [[-a, --alpha], [-i --increment]] [[-s, --fsize], [-n, --isize]] [-h, --help] [-l, --log]"
	echo -e "\n"
	echo -e "\t    --auto\t\t\tAutopilot. Creates alpha or incremental depending on environment"
	echo -e "\t-a, --alpha\t\t\tCreate new alpha. Signals start of new epoch"
	echo -e "\t-i, --increment\t\t\tCreate new incremental"
	#echo -e "\t-z, --omega\t\t\tCreate new omega. Signals end of current epoch"
	echo -e "\t    --installcron\t\tInstall cron and exit. Overwrites any installation if exists\n"
	echo -e "\t-n, --isize\t\t\tShow size of next incremental and exit"
	echo -e "\t-s, --fsize\t\t\tShow size of full MAILDIR and exit"
	echo -e "\t-h, --help\t\t\tShow help and exit"
	echo -e "\t-l, --log\t\t\tSpecify logfile"
	echo -e "\t-p, --push\t\t\tPush incrementals to offsite storage and exit"
	echo -e "\t    --startover\t\t\tRemove all backups if they exist locally and purge timeline and meta files"
	echo -e "\t    --verify\t\t\tVerify files in timeline against local data and timeline"
	echo -e "\t    --healthcheck\t\tVerify files in incremental list against local data and timeline. Intended to be run against local data on the offsite backup system\n"
	echo -e "\nex."
	echo -e "./$(echo $0 | sed 's!.*/!!') --installcron 0:0:*:*:*\t\teveryday at midnight"
	echo -e "./$(echo $0 | sed 's!.*/!!') --installcron=0:0:1:*:*\t\tevery month, on the first day at midnight"
	echo -e "./$(echo $0 | sed 's!.*/!!') --installcron40:5:1:*:*\t\tevery month, on the first day at 5:40AM"
	echo -e "./$(echo $0 | sed 's!.*/!!') --auto"
	echo -e "./$(echo $0 | sed 's!.*/!!') --alpha -l=/path/to/logfile.log"
	echo -e "./$(echo $0 | sed 's!.*/!!') -a --log/path/to/logfile.log"
	echo -e "./$(echo $0 | sed 's!.*/!!') --increment --log /path/to/logfile.log"

	echo -e "\n\nTerminology:"
	echo -e "[ALPHA, alpha]\t\t Initial backup. Equivalent to a full backup of dataset"
	echo -e "[INC, incremental]\t Incremental backup. Utilizes a JSON manifest of filesystem to determine inclusion"
	#echo -e "[OMEGA, omega]\t\t Final incremental backup. Signals creation of new alpha immediately following"

}

#ARGUMENTS : A filesystem string specifying a file list 
#RETURNS : Pruned file list, less the ignore list
cut_ignore() {							#calling cut_ignore with an empty IGNORE_LIST returns the input file
								#calling cut_ignore with a nonexistant IGNORE_LIST returns the input file
								#calling cut_ignore with an empty input parameter returns the input file

	if [ ! -f $1 ]; then write_to_log "ERROR" "File list ${1} passed to cut_ignore does not exist! Outputting empty list..." ; echo "" ; return ; fi
	if [ ! -s $1 ]; then write_to_log "ERROR" "File list ${1} passed to cut_ignore is empty! Returning empty list ${1} ..." ; cat $1 ; return ; fi
	if [ ! -f $IGNORE_LIST ]; then write_to_log "INFO" "Ignore list ${IGNORE_LIST} passed to cut_ignore does not exist! Returning full list ${1} ..." ; cat $1 ; return ; fi

	ll="/tmp/rmll_cutign"					#calling cut_ignore with an empty IGNORE_LIST or input parameter outputs an empty file
	ret="$(sed -e 's/$/d/' <<<$(egrep -n -f $IGNORE_LIST $1 | sed 's/:.*//') | tee $ll)"	#make regex batch to remove
	if [[ $ret == "d" ]]; then 
		cat $1						#if nothing to remove then return full list
	else
		sed -f $ll $1					#execute regex batch and print to stdout
	fi
	rm -f $ll	#remove temporary list
}

#ARGUMENTS : None
#RETURNS : None
make_full_list() {
		
	if [ -f $FULL_LIST ]; then
		cat $FULL_LIST >$PREV_FULL_LIST
	fi

	tlist="/tmp/tlist_mflist"
	find $MAIL_ROOT -type d ! -readable -prune -o -type f >$tlist		#make full list
	cut_ignore $tlist >$FULL_LIST						#prune full list
	rm -f $tlist								#remove temporary list
}

#ARGUMENTS : None
#RETURNS : None
make_inc_list() {
	
	if [ ! -f $TIMELINE ]; then write_to_log "ERROR" "Timeline file ${TIMELINE} does not exist! Exiting..." ; cleanup_pid ; exit_script ; fi	
	if [ ! -s $TIMELINE ]; then write_to_log "ERROR" "Timeline file ${TIMELINE} is corrupt! Empty file! Exiting..." ; cleanup_pid ; exit_script ; fi

	if [ -f $INC_LIST ]; then
		cat $INC_LIST >$PREV_INC_LIST
	fi

	make_full_list								#make full file list. need this full context to determine if files deleted
	tlist="/tmp/tlist_milist1"						#4 temporary lists. 1st made from find. 2nd is output from cut_ignore.
	tlist2="/tmp/tlist_milist2"						#3rd list is 2nd, less identical touched files. 4th list is deleted files
	tlist3="/tmp/tlist_milist3"
	tlist4="/tmp/tlist_milist4"
	find $MAIL_ROOT -type d ! -readable -prune -o -type f -newermt "$(date -d@$(cat $STOPPED) '+%Y-%m-%d %H:%M:%S')" ! -newermt "$(date -d@$(cat $STARTED) '+%Y-%m-%d %H:%M:%S')" >$tlist	#make inc list
	cut_ignore $tlist >$tlist2						#prune incremental list to temporary file
	cat $tlist2 >$tlist3							#make a copy. one list utilized by while loop, so can't be removing indexes
	touch $tlist4								#touch deleted list
	rm -f $tlist								#remove original temporary list

	while read LINE	; do
		if [ ! -f $LINE ]; then write_to_log "ERROR" "File list ${tlist2} passed to make_inc_list has nonexistent file : ${LINE} . Exiting..." ; cleanup_pid ; exit_script ; fi
		hash_json_f="$(md5sum $LINE | sed 's/ .*//')"
		exists_in_tl="$(LINE=$LINE jq -r '.[env.LINE]' $TIMELINE)"
		if [[ $exists_in_tl != "null" ]]; then					#if file from inc list is found in timeline
			hash_json_tl="$(LINE=$LINE jq -r '.[env.LINE].MD5' $TIMELINE)"
			if [[ $hash_json_tl != $hash_json_f ]]; then			#if checksum is not identical in existing file, print log
				write_to_log "INFO" "${LINE} found with NEW checksum. Original=${hash_json_tl} ; New=${hash_json_f}"
			else
				sed -i "\*${LINE}*d" $tlist3		#if checksum is identical, then mtime was touched. No need to update.
			fi						#remove from inc list
		else

			#pruning below to remove old files that may have been moved to ensure a copy is not restored.
			tl_key_if_hash_exists="$(hash_json_f=$hash_json_f jq -r 'to_entries[] | select(.value.MD5==env.hash_json_f) | .key' $TIMELINE)"
			if [[ ! -z $tl_key_if_hash_exists ]] && [ ! -f $tl_key_if_hash_exists ]; then
				echo "${tl_key_if_hash_exists}.deleted" >>$tlist4		#if a matching checksum found, and the filenames are not
			fi									#the same, and the file in the timeline doesn't exist
												#then mark file as deleted. Handling of moved files
											#Dovecot renames (moves) files all the time
											#https://cr.yp.to/proto/maildir.html
		fi
	done < $tlist2

	sorted_fl="/tmp/sorted_flist_milist"
	sort $FULL_LIST >$sorted_fl
	unique_to_tl_list=($(jq -r 'keys_unsorted[] as $k | "\($k)"' $TIMELINE | sort | comm -23 - $sorted_fl))
	for (( i=0;i<${#unique_to_tl_list[@]};i++ )); do
		utl_list_key="${unique_to_tl_list[${i}]}"
		if ! grep -q "${utl_list_key}.deleted" $tlist4 ; then
			echo "${utl_list_key}.deleted" >>$tlist4
		fi
	done									#if the list doesn't already contain
										#then the above recursive test couldn't find a matching moved
										#the only other alternative would be that the file was deleted


	#prepend moved/deleted list to temporary incremental list to create final
	cat $tlist4 $tlist3 >$INC_LIST
	
	if [ $DEBUG -eq 1 ]; then write_to_log "DEBUG" ".deleted list prepared by make_inc_list" ; cat $tlist4 >>$LOGFILE ; fi
	rm -f $tlist2 ; rm -f $tlist3 ; rm -f $tlist4 ; rm -f $sorted_fl
}

#ARGUMENTS : None
#RETURNS : None
#Updates the timeline file with incremental file list without creating an entirely new full dictionary. I assume timeline rotation handled beforehand
update_timeline() {

	write_to_log "INFO" "Remastering timeline"
	sorted_il="/tmp/sorted_ilist_utimeline"
	sort $INC_LIST >$sorted_il
	
	#obtain list of files unique to incremental and common to both
	#for files unique to incremental, add key/value pair to end of timeline
	#for files common to both, remove old key/value pair and add new key/value pair to end of timeline
	#for files marked as removed in incremental, remove key/value pair from timeline

	IFSBAK=$IFS
	IFS=$'\n'
	addto_tl=($(jq -r 'keys_unsorted[] as $k | "\($k)"' $TIMELINE | sort | comm -13 - $sorted_il | grep -v '.deleted$'))
	both_l=($(jq -r 'keys_unsorted[] as $k | "\($k)"' $TIMELINE | sort | comm -12 - $sorted_il | grep -v '.deleted$'))
	remove_l=($(grep '.deleted$' $INC_LIST))
	IFS=$IFSBAK

	for (( h=0;h<${#remove_l[@]};h++ )); do
		jq -r --arg f "${remove_l[${h}]%.deleted}" 'del(.["$f"]) | tostring' $TIMELINE | sponge $TIMELINE
	done

	for (( i=0;i<${#addto_tl[@]};i++ )); do
		i_atol="${addto_tl[${i}]}" ; mtime="$(i_atol=$i_atol jq -r '.[env.i_atol].MTIME' $INC_DICT)" ; md5="$(i_atol=$i_atol jq -r '.[env.i_atol].MD5' $INC_DICT)"
		jq -r --arg f "${i_atol}" --arg m "${mtime}" --arg n "${md5}" '. + {"\($f)": {"MTIME": $m, "MD5": $n}} | tostring' $TIMELINE | sponge $TIMELINE
	done
	
	for (( j=0;j<${#both_l[@]};j++ )); do
		i_bl="${both_l[${j}]}" ; mtime="$(i_bl=$i_bl jq -r '.[env.i_bl].MTIME' $INC_DICT)" ; md5="$(i_bl=$i_bl jq -r '.[env.i_bl].MD5' $INC_DICT)"
		jq -r --arg f "${i_bl}" --arg m "${mtime}" --arg n "${md5}" '. + {"\($f)": {"MTIME": $m, "MD5": $n}} | tostring' $TIMELINE | sponge $TIMELINE
	done
	rm -f $sorted_il
}

#ARGUMENTS : Crontab string, i.e '0:0:1:*:*', where the spaces in the first part of the string are replaced with ':'
#RETURNS : None
write_cron() {

	start_script "INSTALLCRON"
	if [ ! -f $CRONFILE ]; then
		write_to_log "INFO" "CRON file ${CRONFILE} does not exist. Creating..."
		touch $CRONFILE 2>/dev/null ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to create cronfile. Unable to create file ${CRONFILE}" ; exit_script ; fi
		crontab $CRONFILE
	fi

	#parse options provided for CRON 

	regex='^[0-9*,-/]+$'
	IFSBAK=$IFS
	IFS=$':'
	argl=($1)
	IFS=$IFSBAK

	for arg in $argl; do
		if ! [[ $arg =~ $regex ]]; then write_to_log "ERROR" "CRON parsing failed! Parameter '${arg}' is not valid in '${1}'" ; exit_script ; fi
	done

	grep -q $0 $CRONFILE
	if [ $? -eq 0 ]; then
		if [ $(grep $0 $CRONFILE | wc -l) -gt 1 ]; then
			write_to_log "INFO" "Removing all previous CRON installed for ${0} (found $(grep $0 $CRONFILE | wc -l))"
		else
			write_to_log "INFO" "Previous CRON '$(grep $0 $CRONFILE)'"
		fi
	fi	

	write_to_log "INFO" "Updating CRON file ${CRONFILE} to '$(echo $1 | tr ':' ' ') ${0} --auto'"
	sed -i "\!${0}!d" $CRONFILE
	sed -i "\$a$(echo $1 | tr ':' ' ') ${0} --auto" $CRONFILE
	
}

#ARGUMENTS : None
#RETURNS : None
do_recovery() {

	#do_recovery is only called in routines if a lockfile is found, or if a critical error prevented proper backup routines

	cmp -s $STARTED $STOPPED
	if [ $? -ne 0 ]; then		#rotation of data prior to last run
		if [ -f $PREV_FULL_LIST ]; then cat $PREV_FULL_LIST >$FULL_LIST ; fi
		if [ -f $PREV_FULL_DICT ]; then cat $PREV_FULL_DICT >$FULL_DICT ; fi
		if [ -f $PREV_INC_LIST ]; then cat $PREV_INC_LIST >$INC_LIST ; fi
		if [ -f $PREV_INC_DICT ]; then cat $PREV_INC_DICT >$INC_DICT ; fi
		if [ -f $PREV_TIMELINE ]; then cat $PREV_TIMELINE >$TIMELINE ; fi
	fi

	#fix timestamps and contents of control files
	cat $STOPPED >$STARTED
	touch $STARTED -r $STOPPED
	cleanup_lock


	#TODO: might need to remove previous backup and entry from EPOCH_LIST if this method runs 
}

#ARGUMENTS : Run type to classify the log written on error.
#RETURNS : None
run_checks() {

	if [ ! -d $MAIL_ROOT ]; then write_to_log "ERROR" "${1} requested. Root directory ${MAIL_ROOT} does not exist! Exiting..." ; exit_script ; fi
	if [ ! -d $META_DIR ]; then write_to_log "INFO" "Metadata directory ${META_DIR} does not exist! Creating..." ; mkdir -p $META_DIR ; chmod 0755 $META_DIR ; fi
	if [ -f $PIDFILE ]; then write_to_log "INFO" "$(echo $0 | sed 's!.*/!!') appears to be already running! PID file ${PIDFILE} exists! Exiting..." ; exit_script ; fi
	if [ -f $LOCKFILE ]; then write_to_log "INFO" "$(echo $0 | sed 's!.*/!!') appears to have ungracefully stopped the last run. Cleaning up..." ; do_recovery ; fi

	case $1 in
		ALPHA)
			if [ $(whoami) != $(basename $CRONFILE) ]; then write_to_log "ERROR" "${1} requested as euid=$(whoami). Cannot run unless euid=$(basename $CRONFILE) . Exiting..." ; exit_script ; fi
			;;
		INCREMENTAL)
			if [ $(whoami) != $(basename $CRONFILE) ]; then write_to_log "ERROR" "${1} requested as euid=$(whoami). Cannot run unless euid=$(basename $CRONFILE) . Exiting..." ; exit_script ; fi
			if [ ! -f $EPOCH_LIST ]; then write_to_log "ERROR" "${1} requested. Epoch list ${EPOCH_LIST} does not exist! Exiting..." ; exit_script ; fi
			if [ ! -f $PUSH_LIST ]; then write_to_log "ERROR" "${1} requested. Push list ${PUSH_LIST} does not exist! Exiting..." ; exit_script ; fi
			if [ ! -f $STOPPED ]; then write_to_log "ERROR" "${1} requested. Stopped file ${STOPPED} does not exist! No frame of reference! Exiting..." ; exit_script ; fi
			;;
		FULL_SZ)
			;;
		INC_SZ)
			if [ ! -f $STOPPED ]; then write_to_log "ERROR" "${1} requested. Stopped file ${STOPPED} does not exist! No frame of reference! Exiting..." ; exit_script ; fi
			;;
		PUSH)
			if [ ! -f $PUSH_LIST ]; then write_to_log "ERROR" "${1} requested. Push list ${PUSH_LIST} does not exist! Exiting..." ; exit_script ; fi
			if [ ! -s $PUSH_LIST ]; then write_to_log "INFO" "${1} requested. Push list ${PUSH_LIST} is empty! Exiting..." ; exit_script ; fi
			;;
		VERIFY)
			if [ ! -f $TIMELINE ]; then write_to_log "ERROR" "${1} requested. No timeline to verify against! Exiting..." ; exit_script ; fi
			;;
		HEALTHCHK)
			if [ ! -f $TIMELINE ]; then write_to_log "ERROR" "${1} requested. No timeline to healthcheck against! Exiting..." ; exit_script ; fi
			if [ ! -f $INC_LIST ]; then write_to_log "ERROR" "${1} requested. No incremental list to healthcheck against! Exiting..." ; exit_script ; fi
			;;
		*)  write_to_log "ERROR" "Argument ${1} provided to run_checks. Internal error! Exiting..." ; exit_script ;;
	esac

}

#ARGUMENTS : Fullpath of file to encrypt
#RETURNS : Return code specifying success/failure
do_encrypt() {

	if [ ! -z $GPG_RECIPIENT ] && [ $GPG_RECIPIENT != "null" ] && [ $GPG_ENABLED -ne 0 ]; then
		gpg -r $GPG_RECIPIENT -o "${1}.gpg" -e $1
		if [ $? -eq 0 ]; then
			rm -f $1 ; echo "$(basename "${1}.gpg")" >>$PUSH_LIST ; echo "0"
		else
			write_to_log "ERROR" "Unable to encrypt backup ${1} . Rolling back..."
			rm -f $1 ; sed -i "/$(basename $1)/d" $EPOCH_LIST
			do_recovery ; echo "1"
		fi
	else
		echo "$(basename "${1}")" >>$PUSH_LIST
		write_to_log "INFO" "Skipping encryption. gpg_enabled=${GPG_ENABLED} gpg_recipient=${GPG_RECIPIENT}" ; echo "0"
	fi
}

#ARGUMENTS : None
#RETURNS : None
do_push() {

	run_checks "PUSH" ; start_script "PUSH"
	while read LINE ; do
		if [ $(whoami) == $(basename $CRONFILE) ]; then
			if [ $IS_INTERACTIVE -ne 0 ]; then
				rsync -a --chmod=D0770,F0660 --timeout=60 "${META_DIR}/backups/${LINE}" "${GPG_RECIPIENT}:${REMOTE_PATH}"
				if [ $? -eq 0 ]; then write_to_log "INFO" "${LINE} pushed to ${GPG_RECIPIENT} successfully" ; rm -f "${META_DIR}/backups/${LINE}" ; sed -i "/${LINE}/d" $PUSH_LIST ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to remove file from push list ${PUSH_LIST}" ; fi ; else write_to_log "ERROR" "Unable to push ${LINE} to ${GPG_RECIPIENT}. Exiting..." ; exit_script ; fi
			else
				rsync -Pav --chmod=D0770,F0660 --timeout=60 "${META_DIR}/backups/${LINE}" "${GPG_RECIPIENT}:${REMOTE_PATH}"
				if [ $? -eq 0 ]; then write_to_log "INFO" "${LINE} pushed to ${GPG_RECIPIENT} successfully" ; rm -f "${META_DIR}/backups/${LINE}" ; sed -i "/${LINE}/d" $PUSH_LIST ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to remove file from push list ${PUSH_LIST}" ; fi ; else write_to_log "ERROR" "Unable to push ${LINE} to ${GPG_RECIPIENT}. Exiting..." ; exit_script ; fi
			fi
		else
			sudo -u $(basename $CRONFILE) bash --noprofile -c "rsync -Pav --chmod=D0770,F0660 --timeout=60 "${META_DIR}/backups/${LINE}" "${GPG_RECIPIENT}:${REMOTE_PATH}""
			if [ $? -eq 0 ]; then write_to_log "INFO" "${LINE} pushed to ${GPG_RECIPIENT} successfully. uid=$(whoami) euid=$(basename $CRONFILE)" ; sudo -u $(basename $CRONFILE) bash --noprofile -c "rm -f ${META_DIR}/backups/${LINE}" ; sudo -u $(basename $CRONFILE) bash --noprofile -c "sed -i "/${LINE}/d" ${PUSH_LIST}" ; if [ $? -ne 0 ]; then write_to_log "ERROR" "Unable to remove file from push list ${PUSH_LIST} . uid=$(whoami) euid=$(basename $CRONFILE)" ; fi ; else write_to_log "ERROR" "Unable to push ${LINE} to ${GPG_RECIPIENT} . uid=$(whoami) euid=$(basename $CRONFILE). Exiting..." ; exit_script ; fi
		fi
	done < $PUSH_LIST
}

#ARGUMENTS : None
#RETURNS : None
do_startover() {

	if [ $(whoami) == $(basename $CRONFILE) ]; then
		if [ $IS_INTERACTIVE -ne 0 ]; then
			rm -f "${META_DIR}/backups/"* $PIDFILE $LOCKFILE $EPOCH_LIST $FULL_DICT $FULL_LIST $INC_DICT $INC_LIST $PREV_FULL_DICT $PREV_FULL_LIST $PREV_INC_DICT $PREV_INC_LIST $PUSH_LIST $PREV_TIMELINE $TIMELINE $STARTED $STOPPED $BACKUP_VIRTUAL_USERS_OUTFILE
			if [ $? -eq 0 ]; then write_to_log "INFO" "Processed startover successfully. All backups, meta, and control files purged" ; fi
		else
			for i in "${META_DIR}/backups/*" $PIDFILE $LOCKFILE $EPOCH_LIST $FULL_DICT $FULL_LIST $INC_DICT $INC_LIST $PREV_FULL_DICT $PREV_FULL_LIST $PREV_INC_DICT $PREV_INC_LIST $PUSH_LIST $PREV_TIMELINE $TIMELINE $STARTED $STOPPED $BACKUP_VIRTUAL_USERS_OUTFILE; do
				rm -f "${i}" ; if [ $? ]; then echo "Removed ${i}" ; else echo "Could not remove ${i}" ; fi
			done
			if [ $? -eq 0 ]; then write_to_log "INFO" "Processed startover successfully. All backups, meta, and control files purged" ; fi
		fi
	else
		sudo -u $(basename $CRONFILE) bash --noprofile -c 'for i in '"${META_DIR}/backups/* $PIDFILE $LOCKFILE $EPOCH_LIST $FULL_DICT $FULL_LIST $INC_DICT $INC_LIST $PREV_FULL_DICT $PREV_FULL_LIST $PREV_INC_DICT $PREV_INC_LIST $PUSH_LIST $PREV_TIMELINE $TIMELINE $STARTED $STOPPED $BACKUP_VIRTUAL_USERS_OUTFILE"'; do if [ ! -f "${i}" ]; then echo "Skipping ${i}... file does not exist" ; continue ; fi ; rm -f ${i} ; if [ $? ]; then echo "Removed ${i}" ; else echo "Could not remove ${i}" ; fi ; done'
		if [ $? -eq 0 ]; then write_to_log "INFO" "Processed startover successfully. All backups, meta, and control files purged" ; fi
	fi
}

#ARGUMENTS : None
#RETURNS : Dump of db
do_db_backup() {

	if [ $BACKUP_VIRTUAL_USERS -eq 1 ]; then
		eval $BACKUP_VIRTUAL_USERS_CMD
	fi
}

#ARGUMENTS : None
#RETURNS : None
do_verify() {

	start_script "VERIFY" ; errors=0
	if [ $(whoami) == $(basename $CRONFILE) ]; then
		if [ $IS_INTERACTIVE -ne 0 ]; then
			IFSBAK=$IFS ; IFS=$'\n' ; timeline_keys=( $(jq -r 'keys[]' $TIMELINE) ) ; IFS=$IFSBAK
			for (( i=0;i<${#timeline_keys[@]};i++ )); do
				if [ ! -f ${timeline_keys[${i}]} ]; then write_to_log "WARNING" "VERIFY : File ${prefix}${timeline_keys[${i}]} could not be found!" ; continue ; fi
				temp=$(md5sum ${timeline_keys[${i}]} | sed 's/ .*//') ; temp2=$(key=${timeline_keys[${i}]} jq -r '.[env.key].MD5' $TIMELINE) ; if [[ $temp2 != $temp ]]; then write_to_log "ERROR" "VERIFY : File ${timeline_keys[${i}]} hash computed as ${temp}. Timeline value ${temp2}" ; errors=$((errors+1)) ; fi
			done
			write_to_log "INFO" "VERIFY : Total Errors=$errors"
		else
			IFSBAK=$IFS ; IFS=$'\n' ; timeline_keys=( $(jq -r 'keys[]' $TIMELINE) ) ; IFS=$IFSBAK
			for (( i=0;i<${#timeline_keys[@]};i++ )); do
				if [ ! -f ${timeline_keys[${i}]} ]; then write_to_log "WARNING" "VERIFY : File ${prefix}${timeline_keys[${i}]} could not be found!" ; continue ; fi
				temp=$(md5sum ${timeline_keys[${i}]} | sed 's/ .*//') ; temp2=$(key=${timeline_keys[${i}]} jq -r '.[env.key].MD5' $TIMELINE) ; if [[ $temp2 != $temp ]]; then write_to_stderr "ERROR" "VERIFY : File ${timeline_keys[${i}]} hash computed as ${temp}. Timeline value ${temp2}" ; errors=$((errors+1)) ; fi
			done
			echo "VERIFY : Total Errors=$errors" ; write_to_log "INFO" "VERIFY : Total Errors=$errors"
		fi
	else
		IFSBAK=$IFS ; IFS=$'\n' ; timeline_keys=( $(jq -r 'keys[]' $TIMELINE) ) ; IFS=$IFSBAK
		for (( i=0;i<${#timeline_keys[@]};i++ )); do
			if [ ! -f ${timeline_keys[${i}]} ]; then write_to_log "WARNING" "VERIFY : File ${prefix}${timeline_keys[${i}]} could not be found!" ; continue ; fi
			temp=$(md5sum ${timeline_keys[${i}]} | sed 's/ .*//') ; temp2=$(key=${timeline_keys[${i}]} jq -r '.[env.key].MD5' $TIMELINE) ; if [[ $temp2 != $temp ]]; then write_to_stderr "ERROR" "VERIFY : File ${timeline_keys[${i}]} hash computed as ${temp}. Timeline value ${temp2}" ; errors=$((errors+1)) ; fi
		done
		echo "VERIFY : Total Errors=$errors" ; write_to_log "INFO" "VERIFY : Total Errors=$errors"
	fi	
	exit_script
}

#'prefix' should be set to empty string on the active system. only needs to be defined on offsite backup system.
#only meant to be called from offsite backup system. only verifies checksums from files in incremental list
#ARGUMENTS : None
#RETURNS : None
do_healthcheck() {

	start_script "HEALTHCHK" ; errors=0 ; prefix=""
	IFSBAK=$IFS ; IFS=$'\n' ; inc_keys=( $(cat $INC_LIST | grep -v '.deleted$') ) ; IFS=$IFSBAK
	for (( i=0;i<${#inc_keys[@]};i++ )); do
		if [ ! -f "${prefix}${inc_keys[${i}]}" ]; then write_to_log "ERROR" "HEALTHCHK : File ${prefix}${inc_keys[${i}]} could not be found!" ; exit 1 ; fi
		temp=$(md5sum "${prefix}${inc_keys[${i}]}" | sed 's/ .*//') ; temp2=$(key=${inc_keys[${i}]} jq -r '.[env.key].MD5' $TIMELINE) ; if [[ $temp2 != $temp ]]; then write_to_stderr "ERROR" "HEALTHCHK : File ${prefix}${inc_keys[${i}]} hash computed as ${temp}. Timeline value ${temp2}" ; errors=$((errors+1)) ; fi
	done
	echo "HEALTHCHK : Total Errors=$errors" ; write_to_log "INFO" "HEALTHCHK : Total Errors=$errors"
	if [ $errors -ne 0 ]; then exit 1 ; fi
}

#ARGUMENTS : None
#RETURNS : None
#do_healthcheck() {

	#start_script "HEALTHCHK"
	#if [ $(whoami) == $(basename $CRONFILE) ]; then
		#if [ $IS_INTERACTIVE -eq 0 ]; then
			#thlist1="/tmp/tlist_hlist" ; thlist2="/tmp/tlist_hlist2" ; thdict="/tmp/tdict_hlist"
			#find $MAIL_ROOT -type d ! -readable -prune -o -type f ! -newermt "$(date -d@$(cat $STARTED) '+%Y-%m-%d %H:%M:%S')" >$thlist1
			#cut_ignore $thlist1 >$thlist2 ; rm -f $thlist1
			#make_dict $thlist2 >$thdict ; rm -f $thlist2
		#fi
	#fi
	#return
#}

#ARGUMENTS : None
#RETURNS : None
make_alpha() {

	run_checks "ALPHA"
	echo "$(date +%s)" >$STARTED
	start_script "ALPHA" ; create_pid ; create_lock
	make_full_list ; make_full_dict
	if [ -f $TIMELINE ]; then cat $TIMELINE >$PREV_TIMELINE ; fi
	cat $FULL_DICT >$TIMELINE
	dstamp="$(date +%Y%m%d)"
	randa="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -1)"
	if [ ! -f $EPOCH_LIST ]; then echo "alpha-${dstamp}-${randa}.tgz" >>$EPOCH_LIST ; fi
	do_db_backup
	cat $FULL_LIST | tar --exclude=lost+found --use-compress-program="pigz --quiet --best --recursive" -cpf "${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz" $EPOCH_LIST $TIMELINE $BACKUP_VIRTUAL_USERS_OUTFILE -T - >/dev/null 2>&1 ; rm -f $BACKUP_VIRTUAL_USERS_OUTFILE
	write_to_log "INFO" "New ALPHA made : ${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz"
	write_to_log "INFO" "uncompressed : $(gzip -l "${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f3) bytes"
	write_to_log "INFO" "compressed : $(gzip -l "${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f2) bytes"
	write_to_log "INFO" "compression ratio : $(gzip -l "${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f4)"
	if [ $GPG_ENABLED -eq 1 ]; then
		if [ $(do_encrypt "${META_DIR}/backups/alpha-${dstamp}-${randa}.tgz") -eq 0 ]; then 
			cat $STARTED >$STOPPED ; cleanup_lock ; cleanup_pid 
		else
			cleanup_pid
		fi
	else
		cat $STARTED >$STOPPED ; cleanup_lock ; cleanup_pid
	fi
}

#ARGUMENTS : None
#RETURNS : None
make_incremental() {

	run_checks "INCREMENTAL"
	echo "$(date +%s)" >$STARTED
	start_script "INCREMENTAL" ; create_pid ; create_lock
	make_inc_list ; make_inc_dict
	if [ -f $TIMELINE ]; then cat $TIMELINE >$PREV_TIMELINE ; fi
	update_timeline
	dstamp="$(date +%Y%m%d)"
	randa="$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 16 | head -1)"
	echo "incremental-${dstamp}-${randa}.tgz" >>$EPOCH_LIST
	do_db_backup
	echo "$(sed '\*.deleted*d' $INC_LIST)" | tr ' ' '\n' | tar --exclude=lost+found --use-compress-program="pigz --quiet --best --recursive" -cpf "${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz" $EPOCH_LIST $TIMELINE $BACKUP_VIRTUAL_USERS_OUTFILE $INC_LIST -T - >/dev/null 2>&1 ; rm -f $BACKUP_VIRTUAL_USERS_OUTFILE
	write_to_log "INFO" "New INCREMENTAL : ${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz"
	write_to_log "INFO" "Incremental delta : $(calc_size $INC_LIST) bytes"
	write_to_log "INFO" "uncompressed : $(gzip -l "${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f3) bytes"
	write_to_log "INFO" "compressed : $(gzip -l "${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f2) bytes"
	write_to_log "INFO" "compression ratio : $(gzip -l "${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz" | head -2 | tail -1 | tr -s ' ' ',' | cut -d, -f4)"
	if [ $GPG_ENABLED -eq 1 ]; then
		if [ $(do_encrypt "${META_DIR}/backups/incremental-${dstamp}-${randa}.tgz") -eq 0 ]; then 
			cat $STARTED >$STOPPED ; cleanup_lock ; cleanup_pid
		else
			cleanup_pid
		fi
	else
		cat $STARTED >$STOPPED ; cleanup_lock ; cleanup_pid
	fi
}

#ARGUMENTS : None
#RETURNS : None
#make_omega() {

#}

GETOPTS=`getopt -o nshl:azip --long isize,fsize,installcron:,auto,help,log:,alpha,omega,increment,debug,push,startover,verify,healthcheck -- "$@"`
eval set -- "$GETOPTS"

if [[ $1 == "--" ]] || [[ -v $1 ]]; then
	show_help
	exit
else
	#getopt parsing
	while true ; do
		case $1 in
			-n|--isize)
				run_checks "INC_SZ" ; find $MAIL_ROOT -type d ! -readable -prune -o -type f -newermt "$(date -d@$(cat $STOPPED) '+%Y-%m-%d %H:%M:%S')" ! -newermt "$(date '+%Y-%m-%d %H:%M:%S')" >/tmp/next_inc_tmp ; cut_ignore /tmp/next_inc_tmp >/tmp/next_inc ; calc_size /tmp/next_inc ; rm -f /tmp/next_inc_tmp ; rm -f /tmp/next_inc ; shift ; exit ;;
			-s|--fsize)
				run_checks "FULL_SZ" ; find $MAIL_ROOT -type d ! -readable -prune -o -type f >/tmp/full_tmp ; cut_ignore /tmp/full_tmp >/tmp/full ; calc_size /tmp/full ; rm -f /tmp/full_tmp ; rm -f /tmp/full ; shift ; exit ;;
			--installcron)
				if [ $(echo $2 | sed 's/^[\=\ ]//' | tr -cd ':' | wc -c) -eq 4 ]; then write_cron "$(echo $2 | sed 's/^[\=\ ]//')" ; else write_to_log "ERROR" "Invalid input ${2} provided for CRON install" ; exit ; fi ; shift 2 ; exit_script ;;
			--auto)
				if [ ! -f $TIMELINE ]; then run_checks "ALPHA" ; make_alpha ; else run_checks "INCREMENTAL" ; make_incremental ; fi ; do_push ; shift ; exit_script ;;
			-h|--help)
				show_help ; exit ;;
			-l|--log)
				case $2 in
					"") shift 2 ;;
					*) if [ -d `dirname $(echo $2 | sed 's/^[\=\ ]//')` ]; then LOGFILE=$(echo $2 | sed 's/^[\=\ ]//') ; else echo -e "\033[1;33mWARNING! Using default logfile ${LOGFILE} in place of invalid file $(echo $2 | sed 's/^[\=\ ]//')\033[0m" ; fi ; shift 2 ;;
				esac ;;
			-a|--alpha)
				run_checks "ALPHA" ; make_alpha ; shift ; exit_script ;;
			#-z|--omega)
			#
			-i|--increment)
				run_checks "INCREMENTAL" ; make_incremental ; shift ; exit_script ;;
			--debug)
				DEBUG=1 ; shift ;;
			-p|--push)
				run_checks "PUSH" ; do_push ; shift ; exit_script ;;
			--startover)
				do_startover ; shift ; exit_script ;;
			--verify)
				run_checks "VERIFY" ; do_verify ; shift ; exit_script ;;
			--healthcheck)
				run_checks "HEALTHCHK" ; do_healthcheck ; shift ; exit_script ;;
			--) shift ; break ;;
			*) echo -e "\033[1;31mERROR! Argument ${1} provided to main. Internal error!\n\033[0m" ; exit ;;
		esac
	done
fi
