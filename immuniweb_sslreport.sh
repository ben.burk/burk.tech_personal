#!/bin/bash

#IMMUNIWEB SSL REPORT QUEUE/FETCH

#AUTHOR: Ben Burk
#DATE: 2020.12

#REQUIREMENTS
#jq
#curl
#grep
#sed
#cut
#awk
#dig
#mutt
#date

#if this is run for multiple systems (unique fdqns), allow time between each call, 10-15 min, so that the site doesn't return an empty report

if ! [ $(id -u) = 0 ]; then
	echo -e "\033[31;1mERROR! Immuniweb SSL script started without root permissions!\033[0m"
	exit 1
fi

#User-definable variables:	THESE MUST BE DEFINED BEFORE EXECUTION!
#IMMUNI_CRED_FILE: Full path to file containing immuniweb API data
#	FORMAT:
#
#	USER="USER", USER='USER', or USER=USER
#	PASS="PASS", PASS='PASS', or PASS=PASS
#	SCANS="PROJECT1_ID:smtp.example.com:25", SCANS='PROJECT1_ID:smtp.example.com:25,PROJECT2_ID:smtp.example.com:587' or SCANS=PROJECT1_ID:smtp.example.com:143  ... etc
#

IMMUNI_CRED_FILE=null
#IMMUNI_USER=null
#IMMUNI_PASS=null
#IMMUNI_AUTH_COOKIE=null
declare -A IMMUNI_SCANS
EMAIL_BODY=""
nl=$'\n'

<<'###IMMUNIWEB HAS IMPLEMENTED 2FA VIA EMAIL ONLY FOR FREE ACCTS. NO POINT IN USING ACCTS ANYMORE'
get_user_creds() {

	if [ ! -f $1 ]; then echo "ERROR! ::FATAL - Credentials file $1 does not exist!" ; exit 1 ; fi
	grep -q "^USER=" $1 &>/dev/null
	if [ $? -eq 1 ]; then echo "ERROR! ::FATAL - Credentials file $1 missing USER!" ; exit 1 ; else IMMUNI_USER="$(grep "USER" $1 | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" ; fi
	grep -q "^PASS=" $1 &>/dev/null
	if [ $? -eq 1 ]; then echo "ERROR! ::FATAL - Credentials file $1 missing PASS!" ; exit 1 ; else IMMUNI_PASS="$(grep "PASS" $1 | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" ; fi

}

auth_with_immuniweb() {

	get_user_creds $IMMUNI_CRED_FILE
	IMMUNI_AUTH_COOKIE="$(curl -c - -s -X POST -d "portal_type=customer_portal&login=${IMMUNI_USER}&pass=${IMMUNI_PASS}&actbutton=login&actbutton1=Login" https://portal.immuniweb.com/client/login/ | grep "^.immuniweb.com" | awk '{print $NF}')"

}

logout_from_immuniweb() {

	curl -s -X GET -b "gsess=${1}" https://portal.immuniweb.com/client/logout/
}
###IMMUNIWEB HAS IMPLEMENTED 2FA VIA EMAIL ONLY FOR FREE ACCTS. NO POINT IN USING ACCTS ANYMORE

parse_scans_directive() {

	if [ ! -f $1 ]; then echo "ERROR! ::FATAL - Credentials file $1 does not exist!" ; exit 1 ; fi
	grep -q "^SCANS=" $1 &>/dev/null
	if [ $? -eq 1 ]; then echo "ERROR! ::FATAL - Credentials file $1 missing SCANS!" ; exit 1 ; else IFSBAK=$IFS ; IFS=',' read -r -a t_scans <<< "$(grep "SCANS" $1 | sed "s/.*=//; s/\"/'/g" | cut -f2 -d\')" ; IFS=$IFSBAK ; fi
	for (( i=0;i<${#t_scans[@]};i++ )); do
		ind="$(echo "${t_scans[${i}]}" | sed "s/.*"$(echo "${t_scans[${i}]}" | cut -f1 -d:)"://")"
		IMMUNI_SCANS[${ind}]="$(echo "${t_scans[${i}]}" | cut -f1 -d:)"
	done

}

mail_report() {

	att_arr=("$@") ; echo "${EMAIL_BODY}" | mutt -e 'my_hdr From:from@example.com' -e 'set crypt_use_gpgme=no' -e 'set copy=no' -s "SUBJECT LINE $(date +%F)" -a "${att_arr[@]}" -- to@example.com
}

#auth_with_immuniweb
parse_scans_directive $IMMUNI_CRED_FILE

attachments=()
hosts=() ; for i in "${!IMMUNI_SCANS[@]}"; do hosts+=("${i}"); done
for (( i=0;i<${#hosts[@]};i++ )); do
	ini_resp=$(curl -s -H "Content-Type: application/json; charset=utf-8" -X POST -d "{\"domain\":\""${hosts[${i}]}"\",\"private\":false,\"recheck\":true,\"choosen_ip\":\""$(dig +short `echo "${hosts[${i}]}" | sed 's/:.*//'`)"\",\"token\":\"\"}" "https://www.immuniweb.com/ssl/api/v2/tests/")
	job_id=$(echo "${ini_resp}" | jq -r '.job_id' -)
	if [[ $(echo "${ini_resp}" | jq -r '.status' -) != "test_started" ]]; then EMAIL_BODY="${EMAIL_BODY}ERROR! Immuniweb SSL job failed to queue.${nl}RESPONSE:$(echo "${ini_resp}" | jq -r '.' -)${nl}" ; mail_report ; exit 1 ; fi
	test_status="in_progress"
	while [[ ${test_status} == "in_progress" ]]; do
		job_status_resp=$(curl -s -X POST "https://www.immuniweb.com/ssl/api/v2/tests/jobs/${job_id}")
		test_status=$(echo "${job_status_resp}" | jq -r '.status' -)
		sleep 180
	done

	#fetch json result obj
	ustamp=$(date +%s)
	attachments+=("/tmp/${hosts[${i}]}_${ustamp}.json")
	curl -s -L -o "/tmp/${hosts[${i}]}_${ustamp}.json" -H "Accept: application/json" -X GET "https://www.immuniweb.com/ssl/api/v2/tests/${IMMUNI_SCANS[${hosts[${i}]}]}/"
	EMAIL_BODY="${EMAIL_BODY}${nl}${nl}$(jq '.internals' "/tmp/${hosts[${i}]}_${ustamp}.json")"
done
mail_report ${attachments[@]}
for (( i=0;i<${#attachments[@]};i++ )); do
	rm -f ${attachments[${i}]}
done
#logout_from_immuniweb ${IMMUNI_AUTH_COOKIE}
