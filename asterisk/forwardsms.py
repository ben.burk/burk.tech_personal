#!/usr/bin/python3
"""
forwardsms.py

This is a daemon that triggers a php session to forward any sms received by FreePBX.

Usage:
    forwardsms.py [options]
    forwardsms.py (-h | --help)

Options:
    -h --help            Show this screen
    -t=<interval>        Interval to trigger php session in seconds
"""

import docopt
import requests
import time
import os
import sys
import pwd
from datetime import datetime

def main(args):
    interval = None
    pidfile = '/run/asterisk/forwardsms.pid'
    logfile = '/var/log/asterisk/forwardsms'
    url = 'http://127.0.0.1/forwardsms.php'
    if os.path.isfile(pidfile):
        print('ERROR: Daemon is already running')
        if os.geteuid() == pwd.getpwnam('asterisk').pw_uid:
            with open(logfile, 'a') as f:
                f.write('{0} :: {1}\n'.format(datetime.today().strftime('%Y-%m-%d %H:%M:%S'), 'ERROR: Daemon is already running'))
        sys.exit(1)
    else:
        if os.geteuid() == pwd.getpwnam('asterisk').pw_uid:
            with open(pidfile, 'w') as f:
                f.write(str(os.getpid()))
    if os.geteuid() != pwd.getpwnam('asterisk').pw_uid:
        print('ERROR: Unable to start daemon as {0}. Needs to run as asterisk'.format(pwd.getpwuid(os.geteuid()).pw_name)) ; sys.exit(1)
    if args['-t'] is None:
        interval = 1    #assume interval of 1 second if not specified
    else:
        interval = args['-t']
    while(True):
        try:
            r = requests.get(url)
        except Exception as e:
            with open(logfile, 'a') as f:
                f.write('{0} :: ERROR: {1}\n'.format(datetime.today().strftime('%Y-%m-%d %H:%M:%S'), repr(e)))
        time.sleep(interval)


if __name__ == "__main__":
    version = __doc__.strip().split("\n")[0]
    args = docopt.docopt(__doc__, version=version)
    status = main(args)
    sys.exit(status)
