<?php
//requires PHPMailer
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

require '/var/www/html/PHPMailer/src/Exception.php';
require '/var/www/html/PHPMailer/src/PHPMailer.php';
require '/var/www/html/PHPMailer/src/SMTP.php';

$dbhost='localhost';
$dbuser=INSERT_DB_USER_HERE
$dbpassword=INSERT_DB_PASSWORD_HERE

$conn =  mysqli_connect($dbhost, $dbuser, $dbpassword);
if (!$conn) {
  die("Connection failed. Errno: " . mysqli_connect_errno());
}
mysqli_select_db($conn, 'asterisk');
$schema1="SHOW COLUMNS FROM sms_messages LIKE 'forwarded'";
$schema_res=mysqli_query($conn, $schema1);
$exists = (mysqli_num_rows($schema_res))?true:false;
if(!$exists){
  $add_schema="ALTER TABLE sms_messages ADD COLUMN forwarded int(11) NOT NULL DEFAULT 0";
  $set_all_forwarded="UPDATE sms_messages SET forwarded=1";
  mysqli_query($conn, $add_schema);
  mysqli_query($conn, $set_all_forwarded);
  $logfile = fopen("/var/log/asterisk/forwardsms", "a") or die("Unable to open file!");
  fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   - " . " Detected schema change. Restoring..." . PHP_EOL);
  fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   - " . " Altered schema to add 'forwarded' column to 'sms_messages'" . PHP_EOL);
  fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   - " . " Set all rows to 'forwarded=1' in table 'sms_messages'" . PHP_EOL);
  fclose($logfile);
}
$sql1="SELECT DISTINCT sm.to FROM userman_users uu JOIN sms_routing sr ON uu.id = sr.uid JOIN sms_messages sm ON sr.did = sm.to JOIN sms_custom_forwarding scf ON uu.id = scf.uid WHERE scf.forward = 1 AND sm.forwarded = 0 AND sm.direction='in'";
$reader = mysqli_query($conn, $sql1);

if ($reader) {
    while($row = mysqli_fetch_assoc($reader)) {
    $sql2="SELECT sm.id, sm.from, cmge.displayname, sm.to, sm.body, sm.tx_rx_datetime, uu.email FROM userman_users uu JOIN sms_routing sr ON uu.id = sr.uid JOIN sms_messages sm ON sr.did = sm.to JOIN sms_custom_forwarding scf ON uu.id = scf.uid LEFT JOIN contactmanager_entry_numbers cmen ON sm.from = cmen.stripped LEFT JOIN contactmanager_group_entries cmge ON cmen.entryid = cmge.id WHERE scf.forward = 1 AND sm.forwarded = 0 AND sm.direction='in' AND sm.to = ".$row['to'];
    $reader2 = mysqli_query($conn, $sql2);
    if ($reader2) {
	while($row2 = mysqli_fetch_assoc($reader2)){
	$id=$row2['id'];
	$to=$row2['to'];
	$from=$row2['from'];
	$displayname=$row2['displayname'];
	$body=$row2['body'];
        $dt=$row2['tx_rx_datetime'];
	$email=$row2['email'];
	$attachments = [];

	$sql3="SELECT name, raw FROM sms_media WHERE mid = ".$id;
	$reader3 = mysqli_query($conn, $sql3);
	if ($reader3) {
	    while($row3 = mysqli_fetch_assoc($reader3)){
              array_push($attachments, array($row3['name']=>$row3['raw']));
            }
	} else {
	    $logfile = fopen("/var/log/asterisk/forwardsms", "a") or die("Unable to open file!");
	    fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   -  " . mysqli_error($conn) . PHP_EOL);
	    fclose($logfile);
	}

	if($id != ""){
	  $sql4="SELECT id, smtp_server, smtp_port, smtp_user, smtp_password FROM sms_custom_forwarding_smtp LIMIT 1";
          $reader4 = mysqli_query($conn, $sql4);
	  if ($reader4) {
	    while($row4 = mysqli_fetch_assoc($reader4)){
	      if($row4['id'] != ""){
	        if(is_null($displayname)) {
		    $mys = "to: ".$to."\r\nfrom: ".$from."\r\ntime: ".$dt."\r\nmessage: ".$body;
		} else {
                    $mys = "to: ".$to."\r\nfrom: ".$from." ( ".$displayname." )\r\ntime: ".$dt."\r\nmessage: ".$body;
		}
	        $mail = new PHPMailer();
	        $mail->IsSMTP();
	        $mail->CharSet = 'UTF-8';
	        $mail->Host = $row4['smtp_server'];
	        $mail->SMTPDebug = 0;
	        $mail->SMTPAuth = true;
	        $mail->Port = $row4['smtp_port'];
	        $mail->Username = $row4['smtp_user'];
	        $mail->Password = $row4['smtp_password'];
	        $mail->isHTML(false);
	        $mail->setFrom($row4['smtp_user'] . @INSERT_DOMAIN_HERE, INSERT_DISPLAY_NAME_HERE);
		$mail->addAddress($email);
		if(is_null($displayname)) {
	            $mail->Subject = "sms:   to:".$to . "  from:" . $from;
		} else {
		    $mail->Subject = "sms:   to:".$to . "  from:" . $displayname;
		}
		$mail->Body = $mys;
		foreach($attachments as $index => $arr){
		  foreach($arr as $k => $v){
		    $tmp = explode(".", $k);
		    $ext = end($tmp);
		    if ($ext != "smil" && $k != "smil.xml") {
		      $mail->addStringAttachment($v, preg_replace('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}-/', '', $k));
		    }
                  }
		}
	        $mail->send();
		mysqli_query($conn, "UPDATE sms_messages SET forwarded='1' WHERE id='".$id."'");
                unset($attachments);
              }
	    }
	  } else {
	    $logfile = fopen("/var/log/asterisk/forwardsms", "a") or die("Unable to open file!");
	    fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   -  " . mysqli_error($conn) . PHP_EOL);
	    fclose($logfile);
	  }
        }
      }
    } else {
      $logfile = fopen("/var/log/asterisk/forwardsms", "a") or die("Unable to open file!");
      fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   -  " . mysqli_error($conn) . PHP_EOL);
      fclose($logfile);
    }
  }
} else {
  $logfile = fopen("/var/log/asterisk/forwardsms", "a") or die("Unable to open file!");
  fwrite($logfile, date('Y-m-d H:i:s') . "  forwardsms.php   -  " . mysqli_error($conn) . PHP_EOL);
  fclose($logfile);
}
mysqli_close($conn);
?>
