#!/usr/bin/env python
"""
updatehover.py
Logic borrowed from https://gist.github.com/dankrause/5585907

This tool will set/update/delete any record for given (sub)domain, type, and name with content value (if needed i.e. set/update) in your hover.com

Usage:
  updatehover.py (-c <conf> | -u <user> -p <password>) update <type> <name> <content>
  updatehover.py (-c <conf> | -u <user> -p <password>) set <type> <name> <content>
  updatehover.py (-c <conf> | -u <user> -p <password>) delete <type> <name>
  updatehover.py (-h | --help)
  updatehover.py --version

Options:
  -h --help             Show this screen
  --version             Show version
  -c --conf=<conf>      Path to conf
  -u --username=<user>  Your hover username
  -p --password=<pass>  Your hover password
"""

import docopt
import requests
import sys
if (sys.version_info > (3, 0)):
    from configparser import ConfigParser
else:
    from ConfigParser import ConfigParser

class HoverException(Exception):
    pass


class HoverAPI(object):
    def __init__(self, username, password):
        params = {"username": username, "password": password}
        r = requests.post("https://www.hover.com/api/login", json=params)   #change made to be able to login. have to pass creds as json
        if not r.ok or "hoverauth" not in r.cookies:
            raise HoverException(r)
        self.cookies = {"hoverauth": r.cookies["hoverauth"]}
    def call(self, method, resource, data=None):
        url = "https://www.hover.com/api/{0}".format(resource)
        r = requests.request(method, url, data=data, cookies=self.cookies)
        if not r.ok:
            raise HoverException(r)
        if r.content:
            body = r.json()
            if "succeeded" not in body or body["succeeded"] is not True:
                raise HoverException(body)
            return body


def update_dns(username, password, record_type, record_name, record_content):
    try:
        client = HoverAPI(username, password)
    except HoverException as e:
        raise HoverException("Authentication failed")
    
    dns = client.call("get", "dns")
    dns_id = None ; old_record_content = None
    for domain_x in dns["domains"]:
        for entry in domain_x["entries"]:
            if entry["type"] != record_type: continue
            if "{0}.{1}".format(entry["name"], domain_x["domain_name"]) == record_name:
                dns_id = entry["id"] ; old_record_content = entry["content"]
                break
    if dns_id is None:
        raise HoverException("No DNS record found for {0}. Did you mean to set?".format(record_name))

    response = client.call("put", "dns/{0}".format(dns_id), {"content": record_content})
                                                                                                                                                            
    if "succeeded" not in response or response["succeeded"] is not True:
        raise HoverException(response)
    print("Updated {0} {1} : {2} -> {3}".format(record_name, record_type, old_record_content, record_content))
    
def set_dns(username, password, record_type, record_name, record_content):
    try:
        client = HoverAPI(username, password)
    except HoverException as e:
        raise HoverException("Authentication failed")

    dns = client.call("get", "dns")
    dns_id = None ; name = None ; domain = None
    for domain_x in dns["domains"]:
        for entry in domain_x["entries"]:
            if entry["type"] != record_type: continue
            if "{0}.{1}".format(entry["name"], domain_x["domain_name"]) == record_name:
                dns_id = entry["id"]
                raise HoverException("DNS record found for {0}. Did you mean to update?".format(record_name))
    if dns_id is None:
        name_ind = record_name.rfind('.', 0, record_name.rfind('.'))
        if name_ind == -1: 
            name = record_name ; domain = record_name
        else: 
            name = record_name[0:name_ind] ; domain = record_name[name_ind+1:len(record_name)]

        record = {"name": name, "type": record_type, "content": record_content}
        client.call("post", "domains/{0}/dns".format(domain), record)
        print("Created {0}.{1} {2} {3}".format(record["name"], domain, record["type"], record["content"]))


def delete_dns(username, password, record_type, record_name):
    try:
        client = HoverAPI(username, password)
    except HoverException as e:
        raise HoverException("Authentication failed")

    name = None ; domain = None
    name_ind = record_name.rfind('.', 0, record_name.rfind('.'))
    if name_ind == -1: 
        name = record_name ; domain = record_name
    else: 
        name = record_name[0:name_ind] ; domain = record_name[name_ind+1:len(record_name)]

    records = client.call("get", "domains/{0}/dns".format(domain))["domains"][0]["entries"]
    for record in records:
        if record["type"] == record_type and "{0}.{1}".format(record["name"], domain) == record_name:
            client.call("delete", "dns/{0}".format(record["id"]))
            print("Deleted {0}.{1} {2} {3}".format(record["name"], domain, record["type"], record["content"]))
            return

    raise HoverException("No DNS record found for {0}. Unable to delete".format(record_name)) 


def main(args):
    if args["--username"]:
        username, password = args["--username"], args["--password"]
    else:
        config = ConfigParser()
        config.read(args["--conf"])
        items = dict(config.items("hover"))
        username, password = items["username"], items["password"]

    try:
        if args["update"]:
            update_dns(username, password, args["<type>"], args["<name>"], args["<content>"])
        elif args["set"]:
            set_dns(username, password, args["<type>"], args["<name>"], args["<content>"])
        elif args["delete"]:
            delete_dns(username, password, args["<type>"], args["<name>"])
    except HoverException as e:
        print("Unable to modify DNS: {0}".format(e))
        return 1


if __name__ == "__main__":
    version = __doc__.strip().split("\n")[0]
    args = docopt.docopt(__doc__, version=version)
    status = main(args)
    sys.exit(status)
