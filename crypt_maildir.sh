#!/bin/bash
#THIS SCRIPT IS DESIGNED TO BE CALLED FROM 2 MAIN PERSPECTIVES
#AFTER A USER PASSWORD CHANGE (daemon-called)
#MANUALLY FROM CMDLINE (user-initiated)

if ! [[ $(id -u) = 0 ]]; then
	echo -e "\033[1;31mERROR! crypt_maildir script must be called as root\033[0m"
	exit 1
fi

if [[ "$1" == "" ]]; then
	echo -e "\033[1;31mERROR! crypt_maildir script must be called with a mode, i.e.\tencrypt_chk   decrypt   passwd_change\033[0m"
	exit 1
fi

if [[ "$2" == "" ]]; then
	echo -e "\033[1;31mERROR! crypt_maildir script must be called with a mail user\033[0m"
	exit 1
fi

tty -s ; IS_INTERACTIVE=$?
PREFIX_CMD=					#sudo as user, ex sudo -u USER
PREFIX_LOG="date '+%Y-%m-%d - %H:%M:%S'"
tlist1=( $($PREFIX_CMD doveadm user '*') )
if (! printf '%s\n' "${tlist1[@]}" | grep -xq "$2"); then echo -e "\033[1;31mERROR! crypt_maildir script called with invalid user. User ${2} does not exist!\033[0m" ; exit 1 ; fi
USER=$2
MAILDIR_PATH=					#mail path base, ex /var/mail
PASS=""
ENCRYPT_REPORT=/dev/null
TRIGGER_REPORT=0

mail_report() {

        cat $ENCRYPT_REPORT | mail -r from@example.com -s "MAILDIR FS REPORT: $(date +%F) " to@example.com
        exit
}

#Per dovecot mailing list, there isn't a manual way to encrypt mail in folder-key mode. Below makes use of a temporary IMAP mailbox, tmp_crypt, and moves entire mailboxes into this mailbox, then back to the original mailbox. This effectively encrypts all mail in the original mailbox with the original mailbox's private keys
#ARGUMENTS : Mail user, Mail message src folder (basename), interactive mode
#RETURNS : None
encrypt() {

	if [[ "${3}" == "DAEMON" ]]; then printf "\t...skipping\n">>$ENCRYPT_REPORT ; return ; else printf "\n" ; fi
	if [[ "${3}" == "USER" ]] && [[ "${PASS}" == "" ]]; then printf "Enter password for interactive access: " ; read -s -t 60 PASS ; chk_return=$? ; echo ; if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} prompting for interactive access password. Aborting...\n" ; exit 1 ; fi ; fi
	doveadm_stderr="$($PREFIX_CMD mktemp -t doveadm_XXXXXX-$RANDOM-$$)" ; tmp_remaining=$($PREFIX_CMD sh -c "doveadm mailbox status -u ${1} messages tmp_crypt | sed 's/.*=//' 2>${doveadm_stderr}") ; chk_return=$?
	if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} processing temporary folder. Aborting...\n" ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi	
	if [ $tmp_remaining -ne 0 ]; then for i in {1..10}; do echo "$(eval $PREFIX_LOG) :: WARNING! - crypt_maildir found messages in temporary folder during operation. Waiting..." ; sleep 6 ; tmp_remaining=$($PREFIX_CMD sh -c "doveadm mailbox status -u ${1} messages tmp_crypt | sed 's/.*=//' 2>${doveadm_stderr}") ; chk_return=$? ; if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} processing temporary folder. Aborting...\n" ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi ; if [ $tmp_remaining -eq 0 ]; then echo "$(eval $PREFIX_LOG) :: INFO - finished processing temporary folder. Continuing..." ; break ; fi ; done ; if [ $tmp_remaining -ne 0 ]; then echo "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir found messages in temporary folder during operation. Aborting..." ; rm -f $doveadm_stderr ; exit 1 ; fi ; fi
	$($PREFIX_CMD sh -c "doveadm -o plugin/mail_crypt_private_password=\"${PASS}\" move -u ${1} tmp_crypt mailbox ${2} ALL 2>${doveadm_stderr}") ; chk_return=$?
	if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} moving mailbox ${2} to temporary folder. Aborting...\n" ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi
	$($PREFIX_CMD sh -c "doveadm -o plugin/mail_crypt_private_password=\"${PASS}\" move -u ${1} ${2} mailbox tmp_crypt ALL 2>${doveadm_stderr}") ; chk_return=$?
	if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} moving temporary folder back into mailbox ${2}. Aborting...\n" ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi
	rm -f $doveadm_stderr
}

#ARGUMENTS : Full filepath to mail message. Path to private key.
#RETURNS : None
decrypt() {

	if [ ! -f $1 ]; then echo "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir attempted to decrypt a mail file that does not exist : ${1}" ; exit 1 ; fi
	if [ ! -f $2 ]; then echo "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir provided private key does not exist : ${2}" ; exit 1 ; fi
	tempfile=$(mktemp)
	touch -r $1 $tempfile
	$($PREFIX_CMD doveadm fs get crypt private_key_path=${2}:posix:prefix="$(dirname $1)" $1) >"$(dirname $1)/.tempdecrypted"
	mv "$(dirname $1)/.tempdecrypted" $1
	touch -r $tempfile $1
	chmod 0660 $1 ; chown $(id -u `echo $PREFIX_CMD | awk '{print $NF}'`):$(id -g `echo $PREFIX_CMD | awk '{print $NF}'`) $1
	rm -f $tempfile
}

#ARGUMENTS : Mail user, interactive mode
encrypt_chk() {

	if [[ "${2}" == "DAEMON" ]]; then echo "$(eval $PREFIX_LOG) :: DEBUG - Running encrypt_chk in ${2} mode. Changes WON'T be made" >$ENCRYPT_REPORT ; fi
	if [[ "${2}" == "USER" ]]; then echo "$(eval $PREFIX_LOG) :: DEBUG - Running encrypt_chk in ${2} mode. Changes WILL be made" ; ENCRYPT_REPORT=/dev/stdout ; fi
	echo "$(eval $PREFIX_LOG) :: INFO - START" >>$ENCRYPT_REPORT
	echo "$(eval $PREFIX_LOG) :: INFO - Encryption check... Checking all mail folders for user ${1}" >>$ENCRYPT_REPORT
	doveadm_stderr="$($PREFIX_CMD mktemp -t doveadm_XXXXXX-$RANDOM-$$)"
	tlist2=( $($PREFIX_CMD sh -c "doveadm mailbox list -u ${1} 2>${doveadm_stderr}") ) ; chk_return=$?
	if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} getting mail directories for user ${1}. Aborting...\n" >>$ENCRYPT_REPORT ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi
	if (! printf '%s\n' "${tlist2[@]}" | grep -xq "tmp_crypt") && [[ "${2}" == "USER" ]]; then $($PREFIX_CMD sh -c "doveadm mailbox create -u ${1} tmp_crypt 2>${doveadm_stderr}") ; chk_return=$? ; if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} creating temporary mail folder for user $1. Aborting...\n" >> $ENCRYPT_REPORT ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi ; fi
	for (( i=0;i<${#tlist2[@]};i++ )); do
		fullpath=""
		if [[ "${tlist2[${i}]}" == "INBOX" ]]; then fullpath="${MAILDIR_PATH}/${1}/Maildir/cur" ; else fullpath="${MAILDIR_PATH}/${1}/Maildir/${tlist2[${i}]}/cur" ; fi
		mcount=$($PREFIX_CMD doveadm mailbox status -u $1 messages ${tlist2[${i}]} | sed 's/.*=//') ; if [ $mcount -le 0 ]; then continue ; fi
		tlist3=( $(find $fullpath -type f -exec file -b {} \; | sort | uniq) )
		if [[ $(printf '%s\n' "${tlist3[@]}" | grep -vx "MailCrypt") != "" ]]; then
			if [ $TRIGGER_REPORT -eq 0 ]; then TRIGGER_REPORT=1 ; fi
			printf "$(eval $PREFIX_LOG) :: INFO - Found non-encrypted filetypes in folder %s" "${tlist2[${i}]}" >>$ENCRYPT_REPORT
			encrypt $1 ${tlist2[${i}]} $2
		fi
	done
	if [[ "${2}" == "USER" ]]; then tmpcount=$($PREFIX_CMD doveadm mailbox status -u $1 messages tmp_crypt | sed 's/.*=//') ; if [ $tmpcount -gt 0 ]; then echo "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir found messages in temporary folder during cleanup. Aborting..." ; rm -f $doveadm_stderr ; exit 1 ; else $($PREFIX_CMD sh -c "doveadm mailbox delete -u ${1} tmp_crypt 2>${doveadm_stderr}") ; chk_return=$? ; if [ $chk_return -ne 0 ]; then echo -e "$(eval $PREFIX_LOG) :: ERROR! - crypt_maildir encountered error ${chk_return} deleting temporary mail folder for user $1. Aborting...\n" ; cat $doveadm_stderr ; rm -f $doveadm_stderr ; exit 1 ; fi ; fi ; fi
	PASS="" ; rm -f $doveadm_stderr
	echo "$(eval $PREFIX_LOG) :: INFO - STOP" >>$ENCRYPT_REPORT
	if [ $TRIGGER_REPORT -eq 1 ]; then mail_report ; fi
}


#decrypt_all() {


#}


#passwd_change() {

	
#}



case $1 in
	encrypt_chk) if [ $IS_INTERACTIVE -ne 0 ]; then encrypt_chk $USER 'DAEMON' ; else encrypt_chk $USER 'USER' ; fi
	;;
	decrypt) 
	;;
	passwd_change) 
	;;
	*) echo -e "\033[1;31mERROR! crypt_maildir script called with invalid parameter ${1}\033[0m" ; exit 1
esac
